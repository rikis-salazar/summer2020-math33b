# Miscellaneous handouts

In this section you will find material that is related to the contents we are
discussing during lecture, however, presenting such material during class will
either take extra time (hence reducing the amount of time spent dedicated to
other topics), or will not add anything new to our general discussion.

It is my hope that at some point you go over the contents of these handouts
without feeling pressured to study them as they are not a good fit for a midterm
nor a final exam.

1.  [_Accessing Zoom meetings (no waiting room)_][h1]
1.  [_Discarding solutions of ODEs to obtain the solution of an IVP_][h2]
1.  [_A few things about integrating factors_][h3]

---

[Return to main course website][MAIN]


[h1]: ../zoom-no-waiting-room/
[h2]: ./discarding-solutions/
[h3]: ./integrating-factors/
[h4]: https://www.youtube.com/watch?v=yQq1-_ujXrM

[MAIN]: ..

