# Discarding solutions of _ODEs_ to obtain the solution of an _IVP_

> After struggling with the formatting of a CCLE forum answer to one of the
> questions, I decided to write this handout instead.

---

## The question

On one of the homework forums, the following question was posted:

> "On some of the homework questions from [assignment 1] I noticed that [in the
> solution manual] they only took [a specific version of the general] solution
> instead of [the general solution itself]. Could you explain why this is?
> Specifically, this was the case for [exercise] 2.2.19 in [_Trench_][book2]."

[book2]: http://ramanujan.math.trinity.edu/wtrench/texts/TRENCH_DIFF_EQNS_I.PDF

## The explanation

It has to do with the fact the we are dealing with an _Initial Value Problem_ as
opposed to a more generic _Ordinary Differential Equation_.

Let us pretend for a second that we want to solve the ODE
$$(1+2y)y' = 2x.$$

In this case, separation of variables leads to the family of solutions
$$y + y^{2} = x^{2} + C.$$

We can either complete squares, or invoke the general solution of a quadratic
equation to obtain two families of _explicit_ solutions, namely  
\begin{align}
    y_{C}^{+}(x) & = \dfrac{-1 + \sqrt{4x^{2}+4C + 1}}{2}, \\
    y_{C}^{-}(x) & = \dfrac{-1 - \sqrt{4x^{2}+4C + 1}}{2}.
\end{align}

Notice that since at no point we are imposing any conditions on the values of
the independent variable $x$ nor the dependent variable $y$, we would like to
keep all of our options _open_.

> _Summarizing:_ if the problem was an _ODE_, we could write the solution
> implicitly as $y + y^{2} = x^{2} + C$; or explicitly as in the expressions for
> $y_{C}^{+}$ and $y_{C}^{-}$ above.

Next, let us turn our attention to the _IVP_ consisting of the _ODE_ above plus
the initial condition $y(2) = 0$.

First let us simply plugin the values $y = 0$, $x = 2$, in the implicit family
of solutions. This leads to
$$(0) + (0)^{2} = (2)^{2} + C,$$

which tells us that $C = -4$; this, in turn leads us to the corresponding
functions  
\begin{align}
  y_{(-4)}^{+}(x) &= \dfrac{-1+\sqrt{4x^{2}-15}}{2}, \\
  y_{(-4)}^{-}(x) &= \dfrac{-1-\sqrt{4x^{2}-15}}{2}.
\end{align}

However, notice that when we plugin the value $x = 2$, we get  
\begin{align}
  y_{(-4)}^{+}(2) &= \dfrac{-1+\sqrt{4(2)^{2}-15}}{2} = \dfrac{-1+1}{2} = 0, \\
  y_{(-4)}^{-}(2) &= \dfrac{-1-\sqrt{4(2)^{2}-15}}{2} = \dfrac{-1-1}{2} = -1.
\end{align}

Which hopefully explains why one of the functions, namely $y_{(-4)}^{-}$ needs
to be discarded as it does not satisfy the initial condition.

> _Conclusion:_ the solution to the _IVP_ is given by
> $$y_{sol}(t) = \dfrac{-1+\sqrt{4x^{2}-15}}{2}.$$


## What about the interval of existence?

One might be tempted to conclude that the solution exists on the set
$$S = \{ x \in\mathbf{R}\,:\,|x| \ge \sqrt{15}/2\},$$
because on this set the expression $\sqrt{4x^{2}-15}$ is well defined. However,
this set is not _made up of one piece_. Instead, it is a union of intervals.
Namely  
$$S=\Big(-\infty,-\frac{\sqrt{15}}{2}\Big) \cup
  \Big(\frac{\sqrt{15}}{2},\infty\Big).$$

Since the initial condition has positive $x$ value, we must select the
appropriate component of $S$, which in this case is $(\sqrt{15}/2,\infty).$  

---

[Click here to access a `.pdf` version of this document.][pdf]  

---


[Return to main course website][MAIN]

[pdf]: readme.pdf
[MAIN]: ../..


