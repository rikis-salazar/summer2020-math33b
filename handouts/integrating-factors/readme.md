# A few things about integrating factors

> This is an attempt to clarify a few things about the this often misunderstood
> topic.

---

## The basic idea

Integrating factors are easy to explain. Given the _non-exact_ differential
equation
$$P(x,y)\,dx + Q(x,y)\,dy = 0,$$
one seeks to find a function $\mu=\mu(x,y)$ so that the _more or less
equivalent[^one]_ differential equation
$$\mu(x,y)P(x,y)\,dx + \mu(x,y)Q(x,y)\,dy = 0$$
becomes exact. In principle this amounts to solving the following _Partial
Differential Equation_ (PDE for short)
$$\frac{\partial}{\partial y}\Big(\mu(x,y)P(x,y)\Big) =
\frac{\partial}{\partial x}\Big(\mu(x,y)Q(x,y)\Big).$$
After two applications of the product rule, it becomes
$$\mu(x,y)P_{y}(x,y) + \mu_{y}(x,y)P(x,y) =
\mu(x,y)Q_{x}(x,y) + \mu_{x}(x,y)Q(x,y),$$
where the $x$ and $y$ subscripts denote differentiation with respect to $x$ and
$y$, respectively.

[^one]: The multiplication of the original differential equation times the
  integrating factor might result in the gain/loss of a few solutions.

## Non-uniqueness of integrating factors

Note that if $\mu(x,y)$ is an integrating factor for the equation $P\,dx + Q\,dy
= 0$, then any constant multiple of $\mu$ is also an integrating factor of this
equation. Because of this, one often attempts to find _only one_ such
integrating factor $\mu$. However, the following example illustrates the fact
that not all integrating factors can be obtained from one another simply by
multiplication times a real number.

**Example: multiple integrating factors**

Consider the differential equation
$$y\,dx - x\,dy = 0.$$
It is easy to verify that any of the following functions are integrating
factors:
$$\frac{1}{xy},\quad \frac{1}{x^{2}},\quad \frac{1}{y^{2}},\quad
\frac{1}{x^{2}+y^{2}},$$
except for values of $x$ and $y$ for which the denominators are zero.

**Why is this example relevant?**

Because it shows that there is no hope for a _general_ formula for the
integrating factor that would apply for an arbitrary non-exact differential
equation.


## The best we can do: deal with specific cases

In addition to the complication described above, I have also mentioned a few
times before that solving a PDE is usually a lot harder than an _Ordinary
Differential Equation_ (ODE for short). However, this does not mean we should
give up hope. Here are a few cases when we can tackle the PDE associated to the
integrating factor.

1.  When it is just an ODE. For example, if $\mu_{y}(x,y) = 0$, then $\mu$
    depends on only one variable. Moreover we can rewrite the PDE as
    $$\mu'(x) = \Big(\frac{P_{y}-Q_{x}}{Q}\Big)\mu(x)$$
    which is a linear homogeneous ODE for $\mu$.

    > _Note:_ something similar occurs when $\mu_{x}(x,y)=0$.

1.  When we suspect the integrating factor is _separable_; that is, $\mu(x,y) =
    \alpha(x)\beta(y)$ where $\alpha$ and $\beta$ are functions of only one
    variable.

1.  When we suspect the integrating factor has a very specific composition.


## Specific examples

Since case 1 above was discussed during lecture, let me focus on cases 2 and 3.


### Case 2: a separable integrating factor (2.6.21 in Trench)  

The statement goes along the lines of

> **Exercise:** _Find an integrating factor of the form $\mu(x,y) =
> \alpha(x)\beta(y)$ for the differential equation_
> $$\big(a\cos(xy)-y\sin(x,y)\big)\,dx + \big(b\cos(xy)-x\sin(xy)\big)\,dy = 0.$$

In this case,
\begin{align*}
  P(x,y) & = a\cos(xy)-y\sin(xy), & Q(x,y) & = b\cos(xy)-x\sin(xy), \\
  P_{y}(x,y) & = -ax\sin(xy)-yx\cos(xy)-\sin(xy), &
  Q_{x}(x,y) & = -by\sin(xy)-xy\cos(xy)-\sin(xy),
\end{align*}
and the PDE for the integrating factor
$$\mu(x,y)P_{y}(x,y) + \mu_{y}(x,y)P(x,y) =
\mu(x,y)Q_{x}(x,y) + \mu_{x}(x,y)Q(x,y),$$
simplifies a bit because
$$\mu_{x}(x,y) = \frac{\partial}{\partial x}\big(\alpha(x)\beta(y)\big)
  = \alpha'(x)\beta(y),$$
and
$$\mu_{y}(x,y) = \frac{\partial}{\partial y}\big(\alpha(x)\beta(y)\big)
  = \alpha(x)\beta'(y),$$
where the $^{\prime}$ operator represents the total derivative of the functions
$\alpha$ and $\beta$ with respect to the variables $x$, and $y$, respectively.

This results in one single ordinary differential equation with two unknowns
($\alpha$ and $\beta$)
$$\alpha\beta P_{y} + \alpha\beta' P = \alpha\beta Q_{x} + \alpha'\beta Q,$$
which in our specific case is
\begin{multline*}
\alpha(x)\beta(y)\big(-ax\sin(xy)-yx\cos(xy)-\sin(xy)\big)
+ \alpha(x)\beta'(y) \big(a\cos(xy)-y\sin(xy)\big) \\
= \alpha(x)\beta(y) \big(-by\sin(xy)-xy\cos(xy)-\sin(xy)\big)
+ \alpha'(x)\beta(y) \big(b\cos(xy)-x\sin(xy)\big).
\end{multline*}
After a few algebraic manipulations, this turns into
$$A(x,y)\sin(xy) + B(x,y)\cos(xy) =
  \tilde{A}(x,y)\sin(xy) + \tilde{B}(x,y)\cos(xy),$$
where
\begin{align*}
  A(x,y) & = -\alpha(x)\big(ax\beta(y) + y\beta'(y)\big) - \alpha(x)\beta(y), &
  B(x,y) & = a\alpha(x)\beta'(y) - xy\alpha(x)\beta(y), \\
  \tilde{A}(x,y) & = -\beta(y)\big(by\alpha(x) + x\alpha'(x)\big) - \alpha(x)\beta(y), &
  \tilde{B}(x,y) & = b\alpha'(x)\beta(y) - xy\alpha(x)\beta(y).
\end{align*}
Clearly, one way to make sure the PDE for the integrating factor holds is to set
$A(x,y) = \tilde{A}(x,y)$ and $B(x,y) = \tilde{B}(x,y)$. The former equation
leads to (after multiplication times negative one)
\begin{align*}
  \alpha(x)\big(ax\beta(y) + y\beta'(y)\big) + \alpha(x)\beta(y) &
  = \beta(y)\big(by\alpha(x) + x\alpha'(x)\big) + \alpha(x)\beta(y), \\
\end{align*}
which, a few manipulations later becomes
$$\beta(y)\big(a\alpha(x) - \alpha'(x)\big)x
  + \alpha(x)\big(\beta'(y) - b\beta(y)\big)y = 0.$$
Once again, it is clear that one can satisfy this equation by setting the
coefficients in front of the $x$ and $y$ terms equal to zero. One obvious choice
is to set $\alpha = \beta = 0$ for all values of $x$ and $y$; however, even
though this would give is a trivial integrating factor, namely $\mu(x,y) = 0$,
the resulting differential exact differential equation would only have constant
solutions and would not be equivalent to the original one.

Another possibility is to find $\alpha(x)$ and $\beta(y)$ such that
$$\alpha'(x) = a\alpha(x)\qquad\text{and}\qquad\beta'(y) = b\beta(y).$$
The obvious solutions here are
$$\alpha(x) = \mathrm{e}^{ax}\qquad\text{and}\qquad
  \beta(y) = \mathrm{e}^{by},$$
which lead to the integrating factor
$$\mu(x,y) = \alpha(x)\beta(y) = \mathrm{e}^{ax}\mathrm{e}^{by}.$$
Unsurprisingly, this choice of $\alpha$ and $\beta$ also satisfy the equation
$B(x,y) = \tilde{B}(x,y)$. The common value is $(ab-xy)\mathrm{e}^{ax}\mathrm{e}^{by}$.


### Case 2: another separable integrating factor (2.6.19 in Trench)

As in the previous example, the problem can be stated as

> **Exercise:** _Find an integrating factor of the form $\mu(x,y) =
> \alpha(x)\beta(y)$ for the differential equation_
> $$(3x^{2}y^{3} - y^{2} + y)\,dx + (2x - xy)\,dy = 0.$$

In this case,
\begin{align*}
  P(x,y) & = 3x^{2}y^{3} - y^{2} + y, & Q(x,y) & = 2x - xy, \\
  P_{y}(x,y) & = 9x^{2}y^{2} - 2y + 1, & Q_{x}(x,y) & = 2 - y.
\end{align*}
and the differential equation we want to analyze is
$$\alpha\beta P_{y} + \alpha\beta' P = \alpha\beta Q_{x} + \alpha'\beta Q,$$
which in this particular case is
\begin{equation*}
\alpha(x)\beta(y) (9x^{2}y^{2} - 2y + 1)
+ \alpha(x)\beta'(y) (3x^{2}y^{3} - y^{2} + y)
= \alpha(x)\beta(y) (2 - y)
+ \alpha'(x)\beta(y) (2x - xy).
\end{equation*}

The key observation here is that we can rewrite the equation above as
$$3\alpha(x)\big(3\beta(y) + \beta'(y)y\big)x^{2}y^{2} + h(x,y) = 0,$$
where the function $h(x,y)$ captures all of the remaining lower order terms.

To make sure the first term is zero, we find a particular solution of the ODE
$y\beta'(y) + 3\beta(y) = 0$. Here, $\beta(y) = y^{-3}$ will do. Next, we notice
that since $\beta(y)$ is not defined at $y=0$, multiplying both sides of our
differential equation by $y^{3}$ will not introduce extra solutions to it. This
allows us to focus on the equation $y^{3}h(x,y) = 0$, which in this case is
$$\big(y^{3}\beta(y)\big)\alpha(x)(1-2y)
  + \big(y^{3}\beta'(y)\big)\alpha(x)(y-y^{2})
  + \big(y^{3}\beta(y)\big)\big(\alpha(x)(y-2) + \alpha'(x)(y-2)x\big) = 0.$$
After a few algebraic manipulations we get
$$\alpha(x)(1-2y) - 3y^{-1}\alpha(x)(y-y^{2})
 + \alpha(x)(y-2) + \alpha'(x)(y-2)x = 0,$$
which further reduces to
$$\alpha(x)\big(1 - 2y + 3y - 3 + y - 2\big) + \alpha'(x)(y-2)x = 0,$$
which after simplifications leads to
$$(y-2)\big(2\alpha(x) +x\alpha'(x)\big) = 0.$$
A particular solution of this equation is $\alpha(x) = x^{-2}$, which in
combination with $\beta(y)$ found earlier leads to the choice of integrating
factor
$$\mu(x,y) = \frac{1}{x^{2}y^{3}}.$$


### Case 3: when one suspects $\mu$ has a very specific form

The integrating factor from the previous exercise suggests that when we are
dealing with functions $P(x,y)$ and $Q(x,y)$ that are polynomials of the
variables $x$ and $y$, it might be worth considering the possibility that the
integrating factor has the form
$$\mu(x,y) = x^{m}y^{n},$$
for some values $m$ and $n$ that need to be determined.

Indeed, the following exercise shows that this method is worth exploring:

> **Exercise:** _Solve the differential equation_
> $$(2y^{2} - 6xy)\, dx + (3xy - 4x^{2})\, dy = 0.$$

We consider the equation
$$(2x^{m}y^{n+2} - 6x^{m+1}y^{n+1})\, dx 
  + (3x^{m+1}y^{n+1} - 4x^{m+2}y^{n})\, dy = 0,$$
and ask it to be exact. This happens when
\begin{align*}
  \frac{\partial}{\partial y}\Big(2x^{m}y^{n+2} - 6x^{m+1}y^{n+1}\Big)
  & = \frac{\partial}{\partial x}\Big(3x^{m+1}y^{n+1} - 4x^{m+2}y^{n}\Big) \\
  2(n+2)x^{m}y^{n+1} - 6(n+1)x^{m+1}y^{n}
  & = 3(m+1)x^{m}y^{n+1} - 4(m+2)x^{m+1}y^{n} \\
  \big(2(n+2)y - 6(n+1)x\big)x^{m}y^{n}
  & = \big(3(m+1)y - 4(m+2)x\big)x^{m}y^{n}, \\
\end{align*}
which leads to the $2\times2$ system of equations
\begin{align*}
  2(n+2) & = 3(m+1) \\
  6(n+1) & = 4(m+2).
\end{align*}
This system admits a unique solution, namely $m=n=1$. Therefore a possible
choice of integrating factor is
$$\mu(x,y) = xy.$$

## In conclusion

There is really no _straightforward_ way to tackle this kind of problems. But
rest assured that if I decide to use a problem like this in an exam, it will be
carefully selected. Moreover, one or more hints will be provided to you to make
sure you choose the _correct_ path towards the solution.

---

[Click here to access a `.pdf` version of this document.][pdf]  

---


[Return to main course website][MAIN]

[pdf]: readme.pdf
[MAIN]: ../..


