<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>readme</title>
  <style>
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <link rel="stylesheet" href="../../pandoc.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS_CHTML-full" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="a-few-things-about-integrating-factors">A few things about integrating factors</h1>
<blockquote>
<p>This is an attempt to clarify a few things about the this often misunderstood topic.</p>
</blockquote>
<hr />
<h2 id="the-basic-idea">The basic idea</h2>
<p>Integrating factors are easy to explain. Given the <em>non-exact</em> differential equation <span class="math display">\[P(x,y)\,dx + Q(x,y)\,dy = 0,\]</span> one seeks to find a function <span class="math inline">\(\mu=\mu(x,y)\)</span> so that the <em>more or less equivalent<a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a></em> differential equation <span class="math display">\[\mu(x,y)P(x,y)\,dx + \mu(x,y)Q(x,y)\,dy = 0\]</span> becomes exact. In principle this amounts to solving the following <em>Partial Differential Equation</em> (PDE for short) <span class="math display">\[\frac{\partial}{\partial y}\Big(\mu(x,y)P(x,y)\Big) =
\frac{\partial}{\partial x}\Big(\mu(x,y)Q(x,y)\Big).\]</span> After two applications of the product rule, it becomes <span class="math display">\[\mu(x,y)P_{y}(x,y) + \mu_{y}(x,y)P(x,y) =
\mu(x,y)Q_{x}(x,y) + \mu_{x}(x,y)Q(x,y),\]</span> where the <span class="math inline">\(x\)</span> and <span class="math inline">\(y\)</span> subscripts denote differentiation with respect to <span class="math inline">\(x\)</span> and <span class="math inline">\(y\)</span>, respectively.</p>
<h2 id="non-uniqueness-of-integrating-factors">Non-uniqueness of integrating factors</h2>
<p>Note that if <span class="math inline">\(\mu(x,y)\)</span> is an integrating factor for the equation <span class="math inline">\(P\,dx + Q\,dy = 0\)</span>, then any constant multiple of <span class="math inline">\(\mu\)</span> is also an integrating factor of this equation. Because of this, one often attempts to find <em>only one</em> such integrating factor <span class="math inline">\(\mu\)</span>. However, the following example illustrates the fact that not all integrating factors can be obtained from one another simply by multiplication times a real number.</p>
<p><strong>Example: multiple integrating factors</strong></p>
<p>Consider the differential equation <span class="math display">\[y\,dx - x\,dy = 0.\]</span> It is easy to verify that any of the following functions are integrating factors: <span class="math display">\[\frac{1}{xy},\quad \frac{1}{x^{2}},\quad \frac{1}{y^{2}},\quad
\frac{1}{x^{2}+y^{2}},\]</span> except for values of <span class="math inline">\(x\)</span> and <span class="math inline">\(y\)</span> for which the denominators are zero.</p>
<p><strong>Why is this example relevant?</strong></p>
<p>Because it shows that there is no hope for a <em>general</em> formula for the integrating factor that would apply for an arbitrary non-exact differential equation.</p>
<h2 id="the-best-we-can-do-deal-with-specific-cases">The best we can do: deal with specific cases</h2>
<p>In addition to the complication described above, I have also mentioned a few times before that solving a PDE is usually a lot harder than an <em>Ordinary Differential Equation</em> (ODE for short). However, this does not mean we should give up hope. Here are a few cases when we can tackle the PDE associated to the integrating factor.</p>
<ol type="1">
<li><p>When it is just an ODE. For example, if <span class="math inline">\(\mu_{y}(x,y) = 0\)</span>, then <span class="math inline">\(\mu\)</span> depends on only one variable. Moreover we can rewrite the PDE as <span class="math display">\[\mu&#39;(x) = \Big(\frac{P_{y}-Q_{x}}{Q}\Big)\mu(x)\]</span> which is a linear homogeneous ODE for <span class="math inline">\(\mu\)</span>.</p>
<blockquote>
<p><em>Note:</em> something similar occurs when <span class="math inline">\(\mu_{x}(x,y)=0\)</span>.</p>
</blockquote></li>
<li><p>When we suspect the integrating factor is <em>separable</em>; that is, <span class="math inline">\(\mu(x,y) = \alpha(x)\beta(y)\)</span> where <span class="math inline">\(\alpha\)</span> and <span class="math inline">\(\beta\)</span> are functions of only one variable.</p></li>
<li><p>When we suspect the integrating factor has a very specific composition.</p></li>
</ol>
<h2 id="specific-examples">Specific examples</h2>
<p>Since case 1 above was discussed during lecture, let me focus on cases 2 and 3.</p>
<h3 id="case-2-a-separable-integrating-factor-2.6.21-in-trench">Case 2: a separable integrating factor (2.6.21 in Trench)</h3>
<p>The statement goes along the lines of</p>
<blockquote>
<p><strong>Exercise:</strong> <em>Find an integrating factor of the form <span class="math inline">\(\mu(x,y) = \alpha(x)\beta(y)\)</span> for the differential equation</em> <span class="math display">\[\big(a\cos(xy)-y\sin(x,y)\big)\,dx + \big(b\cos(xy)-x\sin(xy)\big)\,dy = 0.\]</span></p>
</blockquote>
<p>In this case, <span class="math display">\[\begin{align*}
  P(x,y) &amp; = a\cos(xy)-y\sin(xy), &amp; Q(x,y) &amp; = b\cos(xy)-x\sin(xy), \\
  P_{y}(x,y) &amp; = -ax\sin(xy)-yx\cos(xy)-\sin(xy), &amp;
  Q_{x}(x,y) &amp; = -by\sin(xy)-xy\cos(xy)-\sin(xy),
\end{align*}\]</span> and the PDE for the integrating factor <span class="math display">\[\mu(x,y)P_{y}(x,y) + \mu_{y}(x,y)P(x,y) =
\mu(x,y)Q_{x}(x,y) + \mu_{x}(x,y)Q(x,y),\]</span> simplifies a bit because <span class="math display">\[\mu_{x}(x,y) = \frac{\partial}{\partial x}\big(\alpha(x)\beta(y)\big)
  = \alpha&#39;(x)\beta(y),\]</span> and <span class="math display">\[\mu_{y}(x,y) = \frac{\partial}{\partial y}\big(\alpha(x)\beta(y)\big)
  = \alpha(x)\beta&#39;(y),\]</span> where the <span class="math inline">\(^{\prime}\)</span> operator represents the total derivative of the functions <span class="math inline">\(\alpha\)</span> and <span class="math inline">\(\beta\)</span> with respect to the variables <span class="math inline">\(x\)</span>, and <span class="math inline">\(y\)</span>, respectively.</p>
<p>This results in one single ordinary differential equation with two unknowns (<span class="math inline">\(\alpha\)</span> and <span class="math inline">\(\beta\)</span>) <span class="math display">\[\alpha\beta P_{y} + \alpha\beta&#39; P = \alpha\beta Q_{x} + \alpha&#39;\beta Q,\]</span> which in our specific case is <span class="math display">\[\begin{multline*}
\alpha(x)\beta(y)\big(-ax\sin(xy)-yx\cos(xy)-\sin(xy)\big)
+ \alpha(x)\beta&#39;(y) \big(a\cos(xy)-y\sin(xy)\big) \\
= \alpha(x)\beta(y) \big(-by\sin(xy)-xy\cos(xy)-\sin(xy)\big)
+ \alpha&#39;(x)\beta(y) \big(b\cos(xy)-x\sin(xy)\big).
\end{multline*}\]</span> After a few algebraic manipulations, this turns into <span class="math display">\[A(x,y)\sin(xy) + B(x,y)\cos(xy) =
  \tilde{A}(x,y)\sin(xy) + \tilde{B}(x,y)\cos(xy),\]</span> where <span class="math display">\[\begin{align*}
  A(x,y) &amp; = -\alpha(x)\big(ax\beta(y) + y\beta&#39;(y)\big) - \alpha(x)\beta(y), &amp;
  B(x,y) &amp; = a\alpha(x)\beta&#39;(y) - xy\alpha(x)\beta(y), \\
  \tilde{A}(x,y) &amp; = -\beta(y)\big(by\alpha(x) + x\alpha&#39;(x)\big) - \alpha(x)\beta(y), &amp;
  \tilde{B}(x,y) &amp; = b\alpha&#39;(x)\beta(y) - xy\alpha(x)\beta(y).
\end{align*}\]</span> Clearly, one way to make sure the PDE for the integrating factor holds is to set <span class="math inline">\(A(x,y) = \tilde{A}(x,y)\)</span> and <span class="math inline">\(B(x,y) = \tilde{B}(x,y)\)</span>. The former equation leads to (after multiplication times negative one) <span class="math display">\[\begin{align*}
  \alpha(x)\big(ax\beta(y) + y\beta&#39;(y)\big) + \alpha(x)\beta(y) &amp;
  = \beta(y)\big(by\alpha(x) + x\alpha&#39;(x)\big) + \alpha(x)\beta(y), \\
\end{align*}\]</span> which, a few manipulations later becomes <span class="math display">\[\beta(y)\big(a\alpha(x) - \alpha&#39;(x)\big)x
  + \alpha(x)\big(\beta&#39;(y) - b\beta(y)\big)y = 0.\]</span> Once again, it is clear that one can satisfy this equation by setting the coefficients in front of the <span class="math inline">\(x\)</span> and <span class="math inline">\(y\)</span> terms equal to zero. One obvious choice is to set <span class="math inline">\(\alpha = \beta = 0\)</span> for all values of <span class="math inline">\(x\)</span> and <span class="math inline">\(y\)</span>; however, even though this would give is a trivial integrating factor, namely <span class="math inline">\(\mu(x,y) = 0\)</span>, the resulting differential exact differential equation would only have constant solutions and would not be equivalent to the original one.</p>
<p>Another possibility is to find <span class="math inline">\(\alpha(x)\)</span> and <span class="math inline">\(\beta(y)\)</span> such that <span class="math display">\[\alpha&#39;(x) = a\alpha(x)\qquad\text{and}\qquad\beta&#39;(y) = b\beta(y).\]</span> The obvious solutions here are <span class="math display">\[\alpha(x) = \mathrm{e}^{ax}\qquad\text{and}\qquad
  \beta(y) = \mathrm{e}^{by},\]</span> which lead to the integrating factor <span class="math display">\[\mu(x,y) = \alpha(x)\beta(y) = \mathrm{e}^{ax}\mathrm{e}^{by}.\]</span> Unsurprisingly, this choice of <span class="math inline">\(\alpha\)</span> and <span class="math inline">\(\beta\)</span> also satisfy the equation <span class="math inline">\(B(x,y) = \tilde{B}(x,y)\)</span>. The common value is <span class="math inline">\((ab-xy)\mathrm{e}^{ax}\mathrm{e}^{by}\)</span>.</p>
<h3 id="case-2-another-separable-integrating-factor-2.6.19-in-trench">Case 2: another separable integrating factor (2.6.19 in Trench)</h3>
<p>As in the previous example, the problem can be stated as</p>
<blockquote>
<p><strong>Exercise:</strong> <em>Find an integrating factor of the form <span class="math inline">\(\mu(x,y) = \alpha(x)\beta(y)\)</span> for the differential equation</em> <span class="math display">\[(3x^{2}y^{3} - y^{2} + y)\,dx + (2x - xy)\,dy = 0.\]</span></p>
</blockquote>
<p>In this case, <span class="math display">\[\begin{align*}
  P(x,y) &amp; = 3x^{2}y^{3} - y^{2} + y, &amp; Q(x,y) &amp; = 2x - xy, \\
  P_{y}(x,y) &amp; = 9x^{2}y^{2} - 2y + 1, &amp; Q_{x}(x,y) &amp; = 2 - y.
\end{align*}\]</span> and the differential equation we want to analyze is <span class="math display">\[\alpha\beta P_{y} + \alpha\beta&#39; P = \alpha\beta Q_{x} + \alpha&#39;\beta Q,\]</span> which in this particular case is <span class="math display">\[\begin{equation*}
\alpha(x)\beta(y) (9x^{2}y^{2} - 2y + 1)
+ \alpha(x)\beta&#39;(y) (3x^{2}y^{3} - y^{2} + y)
= \alpha(x)\beta(y) (2 - y)
+ \alpha&#39;(x)\beta(y) (2x - xy).
\end{equation*}\]</span></p>
<p>The key observation here is that we can rewrite the equation above as <span class="math display">\[3\alpha(x)\big(3\beta(y) + \beta&#39;(y)y\big)x^{2}y^{2} + h(x,y) = 0,\]</span> where the function <span class="math inline">\(h(x,y)\)</span> captures all of the remaining lower order terms.</p>
<p>To make sure the first term is zero, we find a particular solution of the ODE <span class="math inline">\(y\beta&#39;(y) + 3\beta(y) = 0\)</span>. Here, <span class="math inline">\(\beta(y) = y^{-3}\)</span> will do. Next, we notice that since <span class="math inline">\(\beta(y)\)</span> is not defined at <span class="math inline">\(y=0\)</span>, multiplying both sides of our differential equation by <span class="math inline">\(y^{3}\)</span> will not introduce extra solutions to it. This allows us to focus on the equation <span class="math inline">\(y^{3}h(x,y) = 0\)</span>, which in this case is <span class="math display">\[\big(y^{3}\beta(y)\big)\alpha(x)(1-2y)
  + \big(y^{3}\beta&#39;(y)\big)\alpha(x)(y-y^{2})
  + \big(y^{3}\beta(y)\big)\big(\alpha(x)(y-2) + \alpha&#39;(x)(y-2)x\big) = 0.\]</span> After a few algebraic manipulations we get <span class="math display">\[\alpha(x)(1-2y) - 3y^{-1}\alpha(x)(y-y^{2})
 + \alpha(x)(y-2) + \alpha&#39;(x)(y-2)x = 0,\]</span> which further reduces to <span class="math display">\[\alpha(x)\big(1 - 2y + 3y - 3 + y - 2\big) + \alpha&#39;(x)(y-2)x = 0,\]</span> which after simplifications leads to <span class="math display">\[(y-2)\big(2\alpha(x) +x\alpha&#39;(x)\big) = 0.\]</span> A particular solution of this equation is <span class="math inline">\(\alpha(x) = x^{-2}\)</span>, which in combination with <span class="math inline">\(\beta(y)\)</span> found earlier leads to the choice of integrating factor <span class="math display">\[\mu(x,y) = \frac{1}{x^{2}y^{3}}.\]</span></p>
<h3 id="case-3-when-one-suspects-mu-has-a-very-specific-form">Case 3: when one suspects <span class="math inline">\(\mu\)</span> has a very specific form</h3>
<p>The integrating factor from the previous exercise suggests that when we are dealing with functions <span class="math inline">\(P(x,y)\)</span> and <span class="math inline">\(Q(x,y)\)</span> that are polynomials of the variables <span class="math inline">\(x\)</span> and <span class="math inline">\(y\)</span>, it might be worth considering the possibility that the integrating factor has the form <span class="math display">\[\mu(x,y) = x^{m}y^{n},\]</span> for some values <span class="math inline">\(m\)</span> and <span class="math inline">\(n\)</span> that need to be determined.</p>
<p>Indeed, the following exercise shows that this method is worth exploring:</p>
<blockquote>
<p><strong>Exercise:</strong> <em>Solve the differential equation</em> <span class="math display">\[(2y^{2} - 6xy)\, dx + (3xy - 4x^{2})\, dy = 0.\]</span></p>
</blockquote>
<p>We consider the equation <span class="math display">\[(2x^{m}y^{n+2} - 6x^{m+1}y^{n+1})\, dx 
  + (3x^{m+1}y^{n+1} - 4x^{m+2}y^{n})\, dy = 0,\]</span> and ask it to be exact. This happens when <span class="math display">\[\begin{align*}
  \frac{\partial}{\partial y}\Big(2x^{m}y^{n+2} - 6x^{m+1}y^{n+1}\Big)
  &amp; = \frac{\partial}{\partial x}\Big(3x^{m+1}y^{n+1} - 4x^{m+2}y^{n}\Big) \\
  2(n+2)x^{m}y^{n+1} - 6(n+1)x^{m+1}y^{n}
  &amp; = 3(m+1)x^{m}y^{n+1} - 4(m+2)x^{m+1}y^{n} \\
  \big(2(n+2)y - 6(n+1)x\big)x^{m}y^{n}
  &amp; = \big(3(m+1)y - 4(m+2)x\big)x^{m}y^{n}, \\
\end{align*}\]</span> which leads to the <span class="math inline">\(2\times2\)</span> system of equations <span class="math display">\[\begin{align*}
  2(n+2) &amp; = 3(m+1) \\
  6(n+1) &amp; = 4(m+2).
\end{align*}\]</span> This system admits a unique solution, namely <span class="math inline">\(m=n=1\)</span>. Therefore a possible choice of integrating factor is <span class="math display">\[\mu(x,y) = xy.\]</span></p>
<h2 id="in-conclusion">In conclusion</h2>
<p>There is really no <em>straightforward</em> way to tackle this kind of problems. But rest assured that if I decide to use a problem like this in an exam, it will be carefully selected. Moreover, one or more hints will be provided to you to make sure you choose the <em>correct</em> path towards the solution.</p>
<hr />
<p><a href="readme.pdf">Click here to access a <code>.pdf</code> version of this document.</a></p>
<hr />
<p><a href="../..">Return to main course website</a></p>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>The multiplication of the original differential equation times the integrating factor might result in the gain/loss of a few solutions.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
</body>
</html>
