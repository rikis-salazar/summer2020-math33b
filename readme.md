# Math 33B: Differential Equations (Summer 2020)

This is the _MATH_ component of the course website. Here you will find material
related to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus] ([`.pdf` version][syllabus-pdf])
*   [Homework assignments][hw]
*   [Lecture recordings][lec]
*   [Lecture notes][notes]
*   [Course handouts][hand]
*   [**Acessing Zoom meetings (no waiting room)**][zoom]

[syllabus]: syllabus/
[syllabus-pdf]: syllabus/readme.pdf
[hw]: assignments/
[lec]: https://ccle.ucla.edu/course/view.php?id=90004&section=3
[notes]: notes/
[zoom]: zoom-no-waiting-room/
[hand]: handouts/

<!-- This is a comment.
[qs]: https://www.kualo.co.uk/404
-->


## Additional resources

*   [The _CCLE_ component of the course website][CCLE]: it will be used to host
    lecture recordings, discussion forums, quizzes, exams, etc. It also links
    directly to your _Gradescope_ and _Questionly_ accounts.

[CCLE]: https://ccle.ucla.edu/course/view/201C-MATH33B-1
