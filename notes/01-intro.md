---
title: 'Introductory concepts and examples'
author: 'Ricardo Salazar'
date: 'August 04, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Differential equations and solutions

## Ordinary differential equations

::: {.alert title="Definition"} :::
An \textsc{Ordinary Differential Equation} is an equation involving an unknown
function of a single variable together with one or more of its derivatives.
::::::::::::

```


```

For example
$$\frac{dy}{dt} = y - t.$$

Here

*   $y = y(t)$ is the unknown function, and
*   $t$ is the \textsc{Independent Variable}.


## Order of a differential equation

Some other examples of differential equations:
\begin{align*}
  y' & = y^{2} - y \\
  ty' & = y \\
  y' + 4y & = \mathrm{e}^{-3t} \\
  y' & = \cos(ty) \\
  yy'' + t^{2}y & = \cos t \\
  y'' & = y^{2} \\
  \frac{\partial^{2}w}{\partial t^{2}} & = c^{2} \frac{\partial^{2}w}{\partial x^{2}}
\end{align*}

The \textsc{Order} of a differential equation is the order of the highest
derivative that occurs in the equation.


## Normal form

::: {.alert title="Definition"} :::
A first-order differential equation of the form
$$y'=f(t,y)$$
is said to be in \textsc{Normal Form}.
::::::::::::

::: {.alert title="Exercise"} :::
Place the differential equation $t + 4yy' = 0$ into normal form.
::::::::::::

```







```


## Solutions

::: {.alert title="Definition"} :::
A solution of the first-order, ordinary differential equation $\phi(t,y,y') = 0$
is a differentiable function $y(t)$ such that $\phi(t,y(t),y'(t)) = 0$ for all
$t$ in the interval where $y(t)$ is defined.
::::::::::::

::: {.alert title="Exercise"} :::
Show that $y(t) = C\mathrm{e}^{−t^{2}}$ is a solution of the first-order equation
\begin{equation}\label{eq:1}
  y' = −2ty,
\end{equation}
where $C$ is an arbitrary real number.
::::::::::::

```





```


## Solutions (cont)

::: {.alert title="Remarks"} :::
*   The formula $y(t) = C\mathrm{e}^{−t^{2}}$ is called the \textsc{General
    Solution} to equation \eqref{eq:1}.
*   The graphs of these solutions are called \textsc{Solution Curves}.
::::::::::::

Let us draw some pictures...

```










```


## Initial Value Problems

Sometimes we do not want a _whole family_, but a \textsc{Particular Solution}.

::: {.alert title="Exercise"} :::
Given that
\begin{equation}\label{eq:2}
  y(t) = -\frac{1}{t-C}
\end{equation}
is a general solution of $y' = y^{2}$, find a solution satisfying $y(0) = 1$.
::::::::

```








```


## Initial Value Problems (cont)

::: {.alert title="Definition"} :::
A first-order differential equation together with an initial condition,
\begin{equation}\label{eq:3}
  y' = f(t,y),\qquad y(t_{0}) = y_{0},
\end{equation}
is called an \textsc{Initial Value Problem}, or IVP for short.

```



```

A _solution of the IVP_ is a differentiable function $y(t)$ such that

```

```

*   $y'(t) = f(t,y(t))$ for all $t$ in an interval containing $t_{0}$ where
    $y(t)$ is defined, and
*   $y(t_{0}) = y_{0}$.

```




```

::::::::

## Initial Value Problems (cont)

::: {.alert title="Definition"} :::
The \textsc{Interval of Existence} of a solution to a differential equation is
defined to be the largest interval over which the solution can be defined and
remain a solution.
::::::::

::: {.alert title="Example"} :::
Find the interval of existence for the solution to the initial value problem
\begin{equation*}
  y' = y^{2}\quad\text{with}\quad y(0) = 1.
\end{equation*}
::::::::

```







```


## Initial Value Problems (cont)

::: {.alert title="Example"} :::
Verify that $x(s) = 2 − C\mathrm{e}^{−s}$ is a solution of
\begin{equation*}
  x' = 2 − x
\end{equation*}
for any constant $C$. Find the solution that satisfies the initial condition
$x(0) = 1$. What is the interval of existence of this solution?
::::::::

```









```
