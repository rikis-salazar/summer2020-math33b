---
title: 'Existence & Uniqueness of solutions'
author: 'Ricardo Salazar'
date: 'August 17, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## An impossible IVP[^one]

Find a solution of the IVP
\begin{align}\label{impossible:ivp1}
  tx' & = x + 3t^{2}, \\ \label{impossible:ivp2}
  x(0) & = 1.
\end{align}

```












```

[^one]: Moral of the story:


## Existence

### Theorem

Suppose $f(t,x)$ is defined and continuous on a rectangle $R$ defined by
$a < t < b$, and $c < x < d$. Then given any point $(t_{0},x_{0})\in R$, the
initial value problem
\begin{equation}\label{ivp}
  x = f(t,x),\qquad x(t_{0}) = x_{0},
\end{equation}
has a solution $x(t)$ defined in an interval containing $t_{0}$.

```

```

Furthermore, the solution will be defined at least until the solution curve
$t\to\big(t,x(t)\big)$ leaves the rectangle $R$.

```

```

### Impossible IVP

The theorem does not apply because...

```



```


## The rectangle $R$ _vs_ interval of existence[^two]

Find a solution of the IVP
\begin{align*}
  x' & = 1 + x^{2}, \\
  x(0) & = 1.
\end{align*}


```












```

[^two]: For linear equations, the _base_ of $R$ matches the interval of existence.


## Continuity of $f$ is sufficient (not necessary)[^three]

Find a solution of the IVP
\begin{equation*}
  y' = -2y + h(t), \qquad y(0) = 3,
\end{equation*}
where
\begin{equation*}
  h(t) = \begin{cases}  0, & \text{if } t < 1, \\ 5, & \text{otherwise}. \end{cases}
\end{equation*}

```










```

[^three]: Fun fact: some solutions of ODEs are not differentiable!?


## What about uniqueness?

Find a solution of the IVP
\begin{equation*}
  x' = x^{1/3},\qquad x(0) = 0.
\end{equation*}


```













```

## Uniqueness of solutions

### Theorem

Suppose $f(t,x)$, $\partial f/\partial x$ are continuous on
$R = \{a < t < b,\, c < x < d\}$. Suppose $(t_{0},x_{0})\in R$ and that
$$ x' = f(t,x)\qquad\text{and}\qquad y' = f(t,y)$$
satisfy
$$x(t_{0}) = y(t_{0}) = x_{0}.$$

Then as long as $\big(t,x(t)\big)$ and $\big(t,y(t)\big)$ stay in $R$, we have
$x(t) = y(t)$.


### Theorem (stronger versions)

The condition $\partial f/\partial x$ continuous on $R$ can be replaced with

*   $\partial f/\partial x$ _bounded_ in $R$, or
*   $f(t,x)$ _Lipschitz continuous_ on $R$.


## Uniqueness: a typical application

Consider the IVP
\begin{equation*}
  y' = (1-y)\cos(yt),\qquad y(0) = 2.
\end{equation*}

Show that for all time, the solution satisfies $y(t) > 1$.

```












```

