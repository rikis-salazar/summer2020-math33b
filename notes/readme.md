# Lecture notes

This is the place to find _pdf_ files of the documents I use during lecture.

---

<!-- This is a comment 

*   Wednesday: Review and examples (no lecture notes).

*   Tuesday: [2-D non linear systems (reversible systems)][w6-l2].

-->
## Week 6

*   Monday: _Labor day holiday (no lecture)_.

## Week 5

*   Wednesday: [_2x2 systems of equations: distinct roots of characteristic
    polynomial_][w5-l3].

*   Tuesday: [_Intro to systems of 1st order differential equations_][w5-l2].

*   Monday: [_More examples & the Wronskian (continuation)_][w5-l1].


## Week 4

*   Wednesday: [_Variation of parameters & the Wronskian_][w4-l3].

    > _Errata:_ 
    > 
    > *   The notes published here differ from what I wrote during lecture. The
    >     problem (apart from the fact that I ran out of time) is that I used
    >     the symbols $y_{1}$, $y_{2}$, to denote two different things at the
    >     same time, namely, particular solutions of the general homogenous
    >     second order differential equation, as well as the right hand side of
    >     a general $2\times2$ system of algebraic linear equations.

*   Tuesday: [_Undetermined coefficients_][w4-l2].

*   Monday: [_Linear homogeneous equations with constant coefficients_][w4-l1].

    > _Errata:_ 
    > 
    > *   On page 12, the formula for $c_{1}(t)$ at the bottom of the page
    >     should read $c_{1}(t) = at + b$. I incorrectly wrote $c_{1}'(t)$
    >     instead.
    >
    > *   On page 14, the _doodles_ on the right hand side should read $4b^{2} =
    >     4q - p^{2}$.

## Week 3

*   Wednesday: [_Second order equations_][w3-l3].

    > _Errata:_ 
    > 
    > *   On the right side of page 11, when I write ---and say in the
    >     recording-- "linear differential equations", I really mean "linear
    >     **homogeneous** differential equations". The general solution for
    >     inhomogeneous equations has an extra term (we'll talk about this
    >     during week 4).

*   Tuesday: [_Existence and uniqueness_][w3-l2].

    > _Errata:_ 
    > 
    > *   On page 3, at the bottom corner, the equation should read
    >     $$(\tilde{\mu}y)^{\prime} = 0.$$
    >     I wrote $\mu$ instead of $\tilde{\mu}$.
    > *   On page 14, I missed an exponent in the representation of the
    >     two-dimensional plane. The _rectangle_ $\mathcal{R}$ should be
    >     $\mathbb{R}^{2}_{t,y}$ instead of $\mathbb{R}_{t,y}$.

*   Monday: [_Homegeneous equations & Integrating factors_][w3-l1].


## Week 2

*   Wednesday: [_Exact equations_][w2-l3].

*   Tuesday: [_Mixing problems_][w2-l2].

    > _Errata:_ 
    > 
    > *   On page 3, I missed a _prime_ denoting a derivative. The correct
    >     formula should read
    >     $$\text{LHS} = \big( y(t) \big)^{\prime} 
    >       =\big( c(t)\mathrm{e}^{-2t} \big)^{\prime}=\cdots$$

*   Monday: [_Linear differential equations_][w2-l1].


## Week 1

*   Wednesday: [_Separation of variables (continued)_][w1-l3].

*   Tuesday: [_Basic concepts & separable equations_][w1-l2].

    > _Errata:_ 
    > 
    > *   On page 13 (Tuesday, part 1), I incorrectly wrote
    >     $$\phi(t,y,y') = t + 4y'.$$
    >     The correct equation is
    >     $$\phi(t,y,y') = t + 4yy'.$$
    > *   Somewhere in the slides I wrote _groth_ instead of _growth_.

*   Monday: No class notes. Please make sure to read the [syllabus][syl].

[syl]: ../syllabus/
[w1-l2]: 01-tue.pdf
[w1-l3]: 01-wed.pdf

[w2-l1]: 02-mon.pdf
[w2-l2]: 02-tue.pdf
[w2-l3]: 02-wed.pdf

[w3-l1]: 03-mon.pdf
[w3-l2]: 03-tue.pdf
[w3-l3]: 03-wed.pdf

[w4-l1]: 04-mon.pdf
[w4-l2]: 04-tue.pdf
[w4-l3]: 04-wed.pdf

[w5-l1]: 05-mon.pdf
[w5-l2]: 05-tue.pdf
[w5-l3]: 05-wed.pdf

[w6-l1]: 06-mon.pdf
[w6-l2]: 06-tue.pdf

---

[Return to main course website][MAIN]


[MAIN]: ..

