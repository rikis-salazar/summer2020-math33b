---
title: 'Linear Equations'
author: 'Ricardo Salazar'
date: 'August 05, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## Definitions: linear & homogeneous

*   A first-order \textsc{Linear} equation is one of the form
    \begin{equation}\label{lin:eqn}
      x' = a(t)x + f(t).
    \end{equation}

    _Note:_ $x$ also depends on the independent variable; that is, $x=x(t)$.

*   If $f(t) = 0$ in equation \eqref{lin:eqn}, it is also said to be
    \textsc{Homogeneous}.

*   The functions $a(t)$ and $f(t)$ in \eqref{lin:eqn} are called the
    \textsc{Coefficients} of the equation.

*   The equation
    $$b(t)x' = c(t)x + g(t)$$
    is also linear, but one has to be careful when $b(t) = 0$.

## Linear or non-linear

::: {.alert title="Exercise"} :::
Determine whether or not the equations below are linear.
::::::::::::
\begin{align*}
  x' & = t\sin(x) \\[1ex]
  x' & = x\sin(t) \\[1ex]
  y' & = \mathrm{e}^{2t}y + \cos t \\[1ex]
  y' & = yy' \\[1ex]
  y' & = 1 - y^{2} \\[1ex]
  x' & = (3t+2)x + t^{2} - 1
\end{align*}


## Solving 1st order linear homogeneous equations  

::: {.alert title="Exercise"} :::
Find the general solution of \eqref{lin:eqn} for the case when $f(t) = 0$.
::::::::::::

_Hint:_ First order linear homogeneous equations are separable.


```












```


## Solving linear inhomogeneous equations  

::: {.alert title="Exercise"} :::
Find the general solution to the equation
\begin{equation}\label{example:inhom}
  x' = x + \mathrm{e}^{-t}.
\end{equation}
::::::::::::

_Idea:_ Rewrite \eqref{example:inhom} as $x' - x = \mathrm{e}^{-t}$, then find
$\mu(t)$ so that the left hand side of $\mu(t)(x' - x) = \mu(t)\mathrm{e}^{-t}$
becomes the derivative of $\mu(t)x$.


```










```


## Solving linear inhomogeneous equations (cont)

::: {.alert title="Exercise"} :::
Find the general solution of equation \eqref{lin:eqn}.
::::::::::::

_Hint:_ Follow the steps from the previous exercise.

```












```


## ...inhomogeneous equations (general method)

The equation $x' = ax + f$ can be solved using the following steps:

1.  Rewrite as
    $$x' - ax = f.$$

1.  Multiply by \textsc{Integrating Factor}
    $$\mu(t) = \mathrm{e}^{-\int a(t)\,dt}$$
    so that the equation becomes
    $$(\mu x)' = \mu(x' - ax) = \mu f.$$

1.  Integrate to obtain
    $$\mu(t)x(t) = \int u(t)f(t)\,dt + C$$

1.  Solve for $x(t)$.


## Exercises

Solve the Initial Value Problem
\begin{align*}
  x' & = x\sin t + 2t\mathrm{e}^{-\cos t}, \\
  x(0) & = 1.
\end{align*}

```













```


## Exercises

Solve the Initial Value Problem
\begin{align*}
  x' & = x\tan t + \sin t, \\
  x(0) & = 2.
\end{align*}

```













```


## Exercises

Find the general solution of
\begin{align*}
  y' & = -2y + 3.
\end{align*}

_Idea:_ Adapt the solution of the homogeneous equation $y' = -2y$.

```












```


## Variation of parameters

Use the ideas from the previous exercise to find the general solution of \eqref{lin:eqn}.

```















```


## Variation of parameters (general method)

Steps to solve equation \eqref{lin:eqn} via \textsc{Variation of Parameters}:

1.  Let
    $$y_{h}(t) = \mathrm{e}^{\int a(t)\,dt}$$
    be a _particular solution_ of the linear homogeneous equation
    $y_{h}'=ay_{h}$.

1.  Substitute $x = vy_{h}$ into equation \eqref{lin:eqn} to find $v$, or
    remember that
    $$v' = \frac{f}{y_{h}}.$$

1.  Write down the general solution $x(t) = v(t)y_{h}(t)$.
