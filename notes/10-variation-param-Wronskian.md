---
title: 'Variation of parameters & the Wronskian'
author: 'Ricardo Salazar'
date: 'August 26, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## Variation of parameters

### What is it?

Is a technique used to find a _particular solution_ $y_{p}$ to inhomogeneous
linear equations, provided we know the components of the general solution of the
associated homogeneous equation.

```


```

### We've used this before...

*   Take a solution of a _simple_ problem and replace constants $C_{i}$
    ($i=1,2$) with functions $C_{i}(t)$.

*   Substitute this proposed solution into a _less simple_ equation to find an
    ODE for the functions $C_{i}(t)$.

```


```

## Example: 1st order linear equation (revisited)

Use _variation of parameters_ to find the general solution of
$$y' = a(t)y + f(t).$$

_Hint:_ note that $y_{h}(t) = \mathrm{e}^{\int a(t)\,dt}$ satisfies
$y_{h}' = a(t)y_{h}$.[^one]


```










```

[^one]: In other words, $y_{h}$ is a solution of the associated homogeneous
  problem.


## 2nd order linear equations: the general case

To find a particular solution to
$$y'' + p(t)y' + q(t) = g(t),$$
we look for a solution of the form
$$y_{p}(t) = C_{1}(t)y_{1}(t) + C_{2}(t)y_{2}(t),$$
where

*   $C_{1}$, $C_{2}$ are functions to be determined; and

*   $y_{1}$, $y_{2}$ are _good[^two]_ solutions of the associated homogenous
    equation.

```

```

[^two]: We will determine what is the meaning of _good_ along the way.


## Example

Find a particular solution of
\begin{equation*}
  y'' + y = \csc t\qquad (0<t<\pi)
\end{equation*}

```














```



## The _Wronskian_

*   While solving the IVP
    $$x'' + 4x = 0,\qquad x(0) = a,\qquad x'(0) = b,$$
    we arrived at the conclusion that the functions $y_{1}(t) = \cos(2t)$, and
    $y_{2}(t) = \sin(2t)$ needed to satisfy the condition
    \begin{equation}\label{wronsk:point}
      \big(y_{1}(t)y_{2}'(t) - y_{1}'(t)y_{2}(t)\big)\Big|_{t=0} \neq 0.
    \end{equation}

*   In order to succesfully apply variation of parameters we need
    \begin{equation}\label{wronsk:interval}
      y_{1}(t)y_{2}'(t) - y_{1}'(t)y_{2}(t) \neq 0,
    \end{equation}
    on the region we will perform an integral.

It turns out \eqref{wronsk:point} and \eqref{wronsk:interval} are equivalent for
solutions of linear equations.


## The _Wronskian_ (cont)

::: {.alert title="Proposition"} :::
Assume $u$ and $v$ solve
$$y'' + p(t)y' + q(t)y = g(t),$$
in the interval $(\alpha,\beta)$. Then the Wronskian of $u$ and $v$ is either

*   identically equal to zero on $(\alpha,\beta)$; or
*   it ise never equal to zero there.
::::::::::::

```








```

## The _Wronskian_ and linear independence

::: {.alert title="Proposition"} :::
Assume $u$ and $v$ solve
$$y'' + p(t)y' + q(t)y = g(t),$$
in the interval $(\alpha,\beta)$. Then $u$ is a multiple of $v$ if and only if
the Wronskian of $u$ and $v$ is zero.
::::::::::::

```








```

## The main result[^three]

::: {.alert title="Theorem"} :::
Suppose that $y_{1}$ and $y_{2}$ solve the homogeneous equation
\begin{equation}\label{lin:homog}
  y'' + p(t)y' + q(t)y = 0.
\end{equation}
In addition, assume their Wronskian is non-vanishing. Then, the general solution
of \eqref{lin:homog} is
$$y(t) = C_{1}y_{1}(t) + C_{2}y_{2}(t),$$
where $C_{1}$ and $C_{2}$ are arbitrary constants.
::::::::::::

```






```

[^three]: We've using it for quite some time... but we had not proved it.
