---
title: 'Exact Equations'
author: 'Ricardo Salazar'
date: 'August 12, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## Some considerations

*   A differential equation in _normal form_
    $$y' = f(x,y)$$
    can be rewritten as
    \begin{equation}\label{non-sym:ode}
      P(x,y) + Q(x,y)y' = 0.
    \end{equation}

*   _Formally_ let $y' = dy\text{ over }dx$. Write LHS in _symetric_ form[^one]:
    \begin{equation}\label{sym:ode}
      \omega = P(x,y)\,dx + Q(x,y)\,dy.
    \end{equation}
    $\omega$ is known as a \textsc{Differential Form} (or \textsc{1-Form} for short).


[^one]: We can do this because $\frac{1\not6}{\not6 4}=\frac{1}{4}$.


## Exact forms & equations

The 1-form
$$\omega = P(x,y)\,dx + Q(x,y)\,dy$$
is said to be an \textsc{Exact Form} if
\begin{align*}
P(x,y) & = \frac{\partial F}{\partial x}(x,y), &
Q(x,y) & = \frac{\partial F}{\partial y}(x,y),
\end{align*}
for some _continuously differentiable_ function $F(x,y)$.

```

```

The corresponding ODE
$$P(x,y) + Q(x,y)y' = 0$$
is called an \textsc{Exact [Differential] Equation}.


## Exact equations are _easy_ to solve

::: {.alert title="Carefully chosen exercise"} :::
Take for granted that
$$\sin(x+y)\,dx + \big(2y + \sin(x+y)\big)\,dy = 0$$
is exact, then find

*   the general solution of the corresponding ODE; and
*   a solution that passes trough $(\sqrt{\pi},-\sqrt{\pi})$.
::::::::::::

```








```

## Which forms are exact?

::: {.alert title="Theorem"} :::
Let $\omega = P(x,y)\,dx + Q(x,y)\,dy$ be a 1-form where $P$ and $Q$ are
continuously differentiable.

a)  If $\omega$ is exact, then
    \begin{equation}\label{partials:eq}
      \frac{\partial P}{\partial y} = \frac{\partial Q}{\partial x}.
    \end{equation}
a)  If \eqref{partials:eq} holds on a rectangle $R$, then $\omega$ is exact on
    $R$.
::::::::::::

_Sketch of proof:_

```







```


## General method for exact equations

If the ODE $P(x,y) + Q(x,y)y' = 0$ is exact, the solution is given by $F(x,y)=C$
where $F$ is found via the following steps:

1.  Solve $\partial F/\partial x = P$ by integration
    \begin{equation}\label{step:1}
      F(x,y) = \int P(x,y)\,dx + \phi(y).
    \end{equation}

1.  Solve $\partial F/\partial y = Q$ using \eqref{step:1} by choosing $\phi$ so
    that
    \begin{equation}\label{step:2}
      \frac{\partial F}{\partial y} = 
      \frac{\partial}{\partial y} \Big(\int P(x,y)\,dx\Big) + \phi'(y) =
      Q(x,y).
    \end{equation}

Alternatively, one can also _partial integrate_ with respect to $y$ in step 1,
then differentiate with respect to $x$ in step 2.


## Let's practice

Show that
$$\frac{x+2y^{2}}{x^{3}}\,dx - \frac{2y}{x^{2}}\,dy$$
is exact; then solve the corresponding ODE.

```













```

## Integrating factors

*   Not all forms (equations) are exact.

    -   _E.g.:_ $(x+2y^{2})\,dx - 2xy\,dy$ is not exact

    ```

    ```

*   But we might be able to find $\mu(x,y)$ so that
    $$\big(\mu P\big)\,dx + \big(\mu Q\big)\,dy$$
    becomes exact.

    In this case $\mu$ is known as an \textsc{Integrating Factor}.

    -   _E.g.:_ An integrating factor for $(x+2y^{2})\,dx - 2xy\,dy$ is $\mu =
        1/x^{3}$.


## Integrating factors (cont)

::: {.alert title="Separable Equations"} :::
A form/equation of the form
$$P(x)\,dx + Q(y)\,dy = 0$$
is said to be \textsc{Separable}.[^two]

::::::::::::

Find an integrating factor for $y' = f(x) g(y)$.

```







```

[^two]: This is consistent with our _old_ notion of separable ODEs.


## Integrating factors (cont)

::: {.alert title="Linear Equations"} :::
Find an integrating factor for $y' = a(x)y  + f(x)$.
::::::::::::

```













```

## Other cases

::: {.alert title="Definitions"} :::
*   A function $G(x,y)$ is \textsc{Homogeneous Of Degree} $n$ if
    $$G(tx,ty) = t^{n}G(x,y)$$
    for all $t > 0$ and all $x\neq 0$ and $y \neq 0$.

    ```


    ```

*   A form/equation is said to be \textsc{Homogeneous} is both coefficients
    $P(x,y)$ and $Q(x,y)$ are homogeneous of the same degree.[^three]

    ```


    ```
::::::::::::

[^three]: This _homogeneity_ has nothing to do with the one from before.


## Integrating factors (cont)

::: {.alert title="Fun fact"} :::
Homogeneous equations turn separable via $y = xv$.
::::::::::::

::: {.alert title="Example"} :::
Solve the homogeneous equation $(x^{2}+y^{2})\,dx + xy\,dy = 0$.
::::::::::::

```











```


## Integrating factors depending on one variable

### What if $\mu$ was a function of $x$ alone?

```
















```


## Integrating factors depending on one variable

Solve the equation
$$(xy-2)\,dx + (x^{2}-xy)\,dy = 0.$$

_Hint:_ look for an integrating factor depending only on $x$.

```











```
