---
title: 'Linear Homogeneous Equations with Constant Coefficients'
author: 'Ricardo Salazar'
date: 'August 19, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## Constant coefficients + homogeneous

### What we are dealing with

Want to _tackle_ equations of the form
\begin{equation}\label{lin:hom:const}
  y'' + py' + qy = 0
\end{equation}
where $p$ and $q$ are constant.

```

```

> _E.g.:_ A horizontal spring (see earlier notes).

```

```

### Warm up: 1st order equations

Solve the 1st order linear ODE with constant coefficients $y' + py = 0$.

```





```


## Exponential solutions

### The key idea

Let us try $y(t) = \mathrm{e}^{\lambda t}$.

```















```


## Example: distinct real roots ($\lambda_{1} \neq \lambda_{2}$)

### Find general solution to

\begin{equation*}
  y'' - 3y' + 2y = 0
\end{equation*}

```














```


## Example: complex roots ($\lambda_{1} = \overline{\lambda_{2}}$)

### Find general solution to

\begin{equation*}
  y'' + \theta^{2}y = 0,\qquad\text{for constant }\theta.
\end{equation*}

```














```


## Example: complex roots ($\lambda_{1} = \overline{\lambda_{2}}$)

### Find general solution to

\begin{equation*}
  y'' + 2y' + 2y = 0
\end{equation*}

```














```


## Example: repeated roots ($\lambda_{1} = \lambda_{2}$)

### Find general solution to

\begin{equation*}
  y'' - 2y' + y = 0
\end{equation*}

```














```


## Summary

The equation
$$y'' + py' + qy  = 0$$
has \textsc{Characteristic Equation}
$$0 = \lambda^{2} + p\lambda + q =(\lambda - \lambda_{1})(\lambda - \lambda_{2}),$$
where
$$\lambda_{1} = \frac{-p + \sqrt{p^{2} - 4q}}{2},\qquad
  \lambda_{2} = \frac{-p - \sqrt{p^{2} - 4q}}{2}.$$

The general solution is
\begin{equation*}
y(t) =
  \begin{cases}
    C_{1}\mathrm{e}^{\lambda_{1}t} + C_{2}\mathrm{e}^{\lambda_{2}t},
      & \text{if } p^{2} > 4q, \\
    (C_{1} + C_{2}t)\mathrm{e}^{\lambda_{1}t},
      & \text{if } p^{2} = 4q, \\
    \mathrm{e}^{at}\big(C_{1}\cos(bt) + C_{2}\sin(bt) \big),
      & \text{if } p^{2} < 4q.
  \end{cases}
\end{equation*}
Here $\lambda_{1} = a + ib$, and $C_{1}$, $C_{2}$, are arbitrary constants.


## A horizontal spring (revisited)

Recall: horizontal spring system can be modeled by

\begin{equation}\label{hor:spring}
  x'' + \frac{b}{m}x' + \frac{k}{m}x = 0.
\end{equation}

```













```
