---
title: 'Mixing Problems'
author: 'Ricardo Salazar'
date: 'August 05, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## Some considerations

*   Mixing problems often lead to _systems of differential equations_. We'll
    discuss this topic in more detail later during the course.

    -   Simple setups lead to one (or more) linear ordinary differential
        equations.

*   Often overlooked: usually pairs of IVPs are solved.

*   Key assumption: when contents mix instantaneously.

*   A set of _keywords_ determines which of the variables will have its
    derivative appear in the model.

    -   When in doubt, units (_e.g.,_ mass, time, etc.) might help.

*   Most models have the form:
    $$\text{rate of change} = \text{rate in} - \text{rate out}.$$


## Exercise

A tank initially holds 100 gal of pure water. At time $t = 0$, a solution
containing 2 lb of salt per gallon begins to enter the tank **at the rate of** 3
gallons per minute. At the same time a drain is opened at the bottom of the tank
so that the volume of solution in the tank **remains constant**.

How much salt is in the tank after 60 min?

```











```


## Exercise

A 600-gal tank is filled with 300 gal of pure water. A spigot is opened above
the tank and a salt solution containing 1.5 lb of salt per gallon of solution
begins flowing into the tank **at a rate of** 3 gal/min. Simultaneously, a drain
is opened at the bottom of the tank allowing the solution to leave the tank **at
a rate of** 1 gal/min.

What will be the salt content in the tank at the precise
moment that the volume of solution in the tank is equal to the tank’s capacity
(600 gal)?

```









```


## Exercise

Consider two tanks, labeled tank A and tank B. 

*   Tank A contains 100 gal of solution in which is dissolved 20 lb of salt.
*   Tank B contains 200 gal of solution in which is dissolved 40 lb of salt.

    > _Note:_ none of these tanks starts out with pure water.

_Pure water flows_ into tank A **at a rate of** 5 gal/s. There is a drain at the
bottom of tank A. Solution leaves tank A via this drain **at a rate of** 5 gal/s
and flows into tank B **at the same rate**. A drain at the bottom of tank B
allows the solution to leave tank B, also **at a rate of** 5 gal/s.

What is the salt content in tank B after 1 minute?

```





_
```


