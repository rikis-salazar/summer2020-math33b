---
title: 'Inhomogeneous equations & undetermined coefficients'
author: 'Ricardo Salazar'
date: 'August 24, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## Inhomogeneous equations

### In theory

We should be able to write the general solution of

\begin{equation}\label{lin:inhom}
  y'' + p(t)y' + q(t)y = g(t),
\end{equation}

where $p(t)$ and $q(t)$ are continuous.

```

```

### In practice

We will only be able to handle the case where

```

```

*   the coefficients $p(t)$, $q(t)$ are constant; and
*   $g(t)$ belongs to special set of functions.

```

```

To _tackle_ these cases we will use _undetermined coefficients_.


## The main observation

If $\tilde{y}(t)$ and $\hat{y}(t)$ solve the inhomogeneous equation
\begin{equation*}
  y'' + p(t)y' + q(t)y = g(t),
\end{equation*}

then the difference $y_{h}(t) = \tilde{y}(t) - \hat{y}(t)$ solves the homogenous
equation
\begin{equation*}
  y'' + p(t)y' + q(t)y = 0.
\end{equation*}


```










```


## Why do we care?

In particular, this means that any solution $y(t)$ of the inhomogenous
equation \eqref{lin:inhom} can be written as
\begin{equation}\label{inhom:soln}
  y(t) = y_{p}(t) + y_{h}(t),
\end{equation}
where $y_{p}(t)$ is one particular solution of \eqref{lin:inhom}, and $y_{h}(t)$
is a solution of the associated homogeneous problem.

```


```

### Corolary

If we have 

*   **all solutions** of the homogenous problem, and
*   **a single solution** of the inhomogeneous problem,

then we have **all solutions** of the inhomogeneous problem.[^one]

[^one]: This also holds for higher order linear inhomogeneous equations.


## General outline for 2nd order linear inhomogeneous IVPs

*   Find the general solution of the associated homogeneous problem.

    -   If the equation has constant coefficients, propose exponential
        solutions.
    -   If this is not the case, one or more solutions are usually provided.

    In either case, the general solution has the form
    \begin{equation*}
       y_{h} = C_{1}y_{1}(t) + C_{2}y_{2}(t).
    \end{equation*}
    
*   Find one particular solution $y_{p}$ of the inhomogeneous problem.

    -   If the function $g(t)$ is exponential, or polynomial, or a combination
        of _sines_ and _cosines_, undetermined coefficients will work.
    -   $y_{p}$ can also be found via _variation of parameters_.

*   Write down the general solution
    \begin{equation*}
      y(t) = y_{p}(t) + y_{h}(t).
    \end{equation*}

*   Find specific values of $C_{1}$ and $C_{2}$ that satisfy the IVP.


## Undetermined coefficients[^two]

### Inhomogeneous term: exponential

Find the general solution of
\begin{equation*}
  y'' - y' - 2y = 2\mathrm{e}^{-2t}.
\end{equation*}

```











```

[^two]: This only applies when $p(t)$ and $q(t)$ are constant.


## Undetermined coefficients[^two]

### Inhomogeneous term: _sines_ or _cosines_

Find ~~the general~~ a particular solution of
\begin{equation*}
  y'' - 2y' - 3y = 5\sin(3t).
\end{equation*}

```











```


## Undetermined coefficients[^two]

### Inhomogeneous term: polynomials

Solve the IVP
\begin{equation*}
  y'' + 2y' - 3y = 3t + 4,\qquad y(0) = -2,\qquad y'(0) = 3.
\end{equation*}

```











```


## Undetermined coefficients[^two]

### Inhomogeneous term: matching the characteristic roots

Find a particular solution of
\begin{equation*}
  y'' - y' - 2y = 3\mathrm{e}^{-t}.
\end{equation*}

```











```


## Undetermined coefficients[^two]

### Inhomogeneous term: combination of previous cases

Find a particular solution of
\begin{equation*}
  y'' - y' - 2y = 2\mathrm{e}^{-2t} -5\mathrm{e}^{3t}.
\end{equation*}

```











```


## A vertical spring (revisited)

Recall: vertical spring system can be modeled by

\begin{equation}\label{hor:spring}
  x'' + \frac{b}{m}x' + \frac{k}{m}x = g.
\end{equation}

```













```
