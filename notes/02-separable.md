---
title: 'Separable Equations'
author: 'Ricardo Salazar'
date: 'August 04, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Solutions to Separable Equations

## An Exponential Model

The equation
\begin{equation}\label{exp:eqn}
  N' = r N,\quad r\text{ real},
\end{equation}
arises often in applications.

::: {.alert title="Example"} :::
Model the population of a type of bacteria where the _per capita growth rate_ is
constant.
::::::::::::

```









```


## The Exponential Equation

Because of the form of its solutions, equation \eqref{exp:eqn} is called the
\textsc{Exponential Equation}.

::: {.alert title="Exercise"} :::
Find the general solution of 
\begin{equation*}
  N' = r N,\quad r\text{ real}.
\end{equation*}
::::::::::::


```










```


## Separating variables

::: {.alert title="Exercise"} :::
Solve the differential equation
$$y' = ty^{2}.$$
::::::::::::

```












```


## The general method

Equations of the form
$$\frac{dy}{dt} = \frac{g(t)}{h(y)}\qquad \text{or} \qquad \frac{dy}{dt} =
  g(t)f(y)$$
are called \textsc{Separable}.

The general steps to solve them are:

1.  Separate the variables:
    $$\frac{1}{f(y)}\,dy = g(t)\,dt$$

1.  Integrate both sides:
    $$\int\frac{1}{f(y)}\,dy = \int g(t)\,dt$$

1.  Solve for $y(t)$, if possible.


## Newton's law of cooling

::: {.alert title="The statement"} :::
_"The rate of change of an object’s temperature ($T$) is proportional to the
difference between its temperature and the ambient temperature ($A$)."_

In symbols:
$$\frac{dT}{dt} = -k(T-A)$$
::::::::::::

Use separation of variables to find $T(t)$.

```









```


## Newton's law of cooling (cont)

::: {.alert title="Exercise"} :::
A can of beer at 40$^{\circ}$F is placed into a room where the temperature is
70$^{\circ}$F. After 10 minutes the temperature of the beer is 50$^{\circ}$F.

*   What is the temperature of the beer as a function of time?
*   What is the temperature of the beer 30 minutes after the beer was placed
    into the room?
::::::::::::

```










```

## An _impossible_ solution?!

In some cases step 3 in separation of variables fails.

::: {.alert title="Exercise"} :::
_Solve_ $y' = \sin y$, for an arbitrary initial condition $y(0) = y_{0}$.
::::::::::::


```












```


## Implicit _vs_ explicit solutions

::: {.alert title="Definition"} :::
An \textsc{Explicit Solution} of a differential equation is one for which we
have a formula for the unknown as a function of the independent variable.
::::::::::::

For example, an explicit solution for _Newton's law of cooling_ is
$$T(t) = A + (T_{0} - A)\mathrm{e}^{-kt}.$$

```

```

In contrast, the _solution_ found in the previous exercise, namely
$$t = \log\Big|\frac{\csc y_{0} + \cot y_{0}}{\csc y(t) + \cot y(t)}\Big|,$$
is an example of an \textsc{Implicit Solution}.


## Final remark

Separation of variables works because...

```















```
