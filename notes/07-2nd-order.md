---
title: 'Second Order Equations'
author: 'Ricardo Salazar'
date: 'August 19, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

## Definitions

*   A \textsc{Second-Order Differential Equation} is an equation involving the
    independent variable $t$ and an unknown function $y$ along with its first
    and second derivatives.

    Assuming it is possible to _solve_ for $y''$ we write
    \begin{equation}\label{norm:form:2}
      y'' =  f(t,y,y').
    \end{equation}

    ```

    ```

*   A \textsc{Solution} is a twice continuously differentiable function for
    which equation \eqref{norm:form:2} holds.


## Definitions

*   A second-order \textsc{Linear Equation} has of the form
    \begin{equation}\label{lin:eqn}
      y'' + p(t)y' + q(t)y =  g(t).
    \end{equation}

    ```


    ```

*   The functions $p(t)$, $q(t)$, and $g(t)$ in \eqref{lin:eqn} are called
    \textsc{Coefficients}.

    ```


    ```

*   If the function $g(t)$ is identically equal to zero, the equation is
    called \textsc{Homogeneous}.


## Example: linear homogeneous

### A horizontal spring

```
















```


## Example: linear inhomogeneous

### A vertical spring

```
















```


## Existence and uniqueness

All linear equations have unique solutions, provided that the coefficients are
continuous.


```














```


## The space of solutions homogeneous ODEs

::: {.alert title="Proposition"} :::
If $y_{1}$ and $y_{2}$ are both solutions of the homogeneous equation
\begin{equation}\label{lin:hom:eqn}
  y'' + p(t)y' + q(t)y = 0,
\end{equation}
then the function
\begin{equation}\label{sol:hom:eqn}
  y = C_{1}y_{1} + C_{2}y_{2}
\end{equation}
is also a solution of \eqref{lin:hom:eqn} for all values of $C_{1}$ and $C_{2}$.
::::::::::::

_Proof:_

```






```


## Linear algebra stuff

### If it looks like a ~~duck~~ vector space and ~~quacks~~ behaves like a vector space...

```
















```


## A telling example

Find the solution[^one] of
$$x'' + 4x = 0$$
subject to the initial conditions
$$x(0) = 4,\qquad x'(0) = 2.$$

```










```

[^one]: Recall that linear equations with continuous coefficients have unique solutions.
















