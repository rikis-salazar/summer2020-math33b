# Syllabus: Summer 2020 Math 33b Lecture 1

> If you are reading the online (html) version of this syllabus, [click
> here][pdf] to access a `.pdf` version of this document.[^if-you]

[pdf]: readme.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.

---

## Course information

**Contact:** [rsalazar@math.ucla.edu][correo] (write "Math 33b" in the subject).

**Dates & Time:** Monday, Tuesday & Wednesday, from 11:00 to 12:50 pm PDT.

**Location:** Weather permitting we will alternate between _backyard, living
room, and quite possibly a bedroom_.

**Office hours:** Mondays & Thursdays from 9:00 to 10:00 pm PDT.

> For your convenience, a direct link to the Zoom meeting room can be found at
> the [Zoom links section][zoom-links] of the CCLE class website.

**Teaching assistant(s):**

|**Section**| **T. A.**       |**Office**| **E-mail**                            |  
|:---------:|:----------------|:--------:|:--------------------------------------|  
|  1A & 1B  | Bowman, Benjamin |  _N/A_   | [`benbowman314@math.ucla.edu`][ta1]             |  
|    1C     | Snyder, Jason   |  _N/A_   | [`snyder@math.ucla.edu`][ta2]             |  

[correo]: mailto:rsalazar@math.ucla.edu
[zoom-links]: https://ccle.ucla.edu/course/view.php?id=90015&section=4
[ta1]: mailto:benbowman314@math.ucla.edu
[ta2]: mailto:snyder@math.ucla.edu


## Course Description

From the math department [_General Course Outline_][GCO]:

> First-order, linear differential equations; second-order, linear differential
> equations with constant coefficients; power series solutions; linear systems.

[GCO]: https://www.math.ucla.edu/ugrad/courses/math/33B


## Textbook & supplemental references

*   Polking, John C., Boggess, A., Arnold, D., _Differential Equations (2nd
    Ed.)_, Prentice Hall.

*   [Trench, William F. _Elementary Differential Equations_][ref2].

    > A free resource that also come with a student solutions manual. All the
    > topics listed at the [course outline][GCO] can be found therein.
    
[ref2]: http://ramanujan.math.trinity.edu/wtrench/texts/TRENCH_DIFF_EQNS_I.PDF


## Course outline

We will attempt to cover the material in the [mathematics department general
course outline][GCO]. Although an effort will be made to follow these
guidelines, material presented during lecture might be chosen from supplemental
references and/or material readily available online.


## CCLE and MyUCLA

This course will use a combination several different Internet sites (CCLE,
Gradescope, Questionsly, My.Ucla, etc.), as well as other regular (_i.e.,_
non-protected) sites to post course materials and announcements. These materials
can include the syllabus, handouts and Internet links referenced in class.


## Reaching me via email

_During this current quarter I will be in charge of course(s) were enrollment is
higher than usual for a UCLA class._ In practice, this means that emails you
send to my email address might go unanswered for a rather long period of time.
Before sending me a message, you are encouraged to consult the [CCLE Discussion
forum][forum], as well as this syllabus, as your question(s) might already be
answered there.

If you feel that your question/issue has not been addressed in the places listed
above, do feel free to send me a message, however keep the following in mind:

[forum]: https://ccle.ucla.edu/mod/forum/view.php?id=3001003


*   I receive a high volume of messages throughout the day; in most cases it
    will be faster for you to get the information you seek if you reach out to
    me directly via Zoom (say during O.H., or right before/after lecture).

*   Messages with special keywords _skip_ my inbox. Use this to your
    advantage: if you add `Math 33b` to your subject line, your message will
    find its way into a special folder that I check periodically. In most cases
    this reduces the time you have to wait before I reply to it.

    > Special note: the current situation has exasperated the problem with my
    > inbox as I've had to disable many of the email filters that I used in the
    > past. You are therefore encouraged to, whenever possible, reach out to me
    > via Zoom (e.g., right after lecture or during Office Hours), or post your
    > _non-sensitive_ questions to the [CCLE Discussion forum][forum].

*   ~~**Messages with special attachments**, more specifically text files with
    specific file extensions (_e.g.,_ `.cpp`, `.h`, etc.), as well as messages
    containing some types of image attachments (_e.g.,_ `png`, `jpg`, `pdf`,
    etc.), **are sent directly to my _trash_ folder**.~~

    > ~~This inconvenience brought to you by students that are under the
    > impression that deadlines do not apply to them (_e.g.,_ they attempt to
    > _late-submit_ their assignments via email attachments).~~
    >
    > **Update:** I am no longer automatically rejecting the files described
    > above, as they may contain assigmnents and/or answers to exam questions.
    > In practice this means that it will take me a very long period of time to
    > aknowledge your submissions if you do not follow the proper channels
    > (e.g., submissions to Gradescope or CCLE).


## Grading

_Grading method:_ quizzes (**Quiz**), homework assignments (**Hw**), midterms
(**Mid1**, **Mid2**), and final exam (**Final**).

*   _Quizzes:_ there will be **9 quizzes** throughout the quarter. They will be
    based on homework problems assigned prior to the date the quiz is released. 
    The term _"based on homework problems"_ means that I reserve the right to
    make minor modifications on problems that you were supposed to complete
    beforehand, or to assign a completely different problem that can be solved
    using the same ideas/techniques you were expected to develop previously.

    Quizzes will be released through CCLE via the [Quizzes & Exams][q-and-e]
    section. **Quiz 0, as well as the two lowest scores from quizzes 1--8 will
    not count towards the computation of your grade**.

    Each selected problem should be suitable to be completed in about 10
    minutes, but a time window of 15 minutes will be set to account for the time
    it takes to upload your work to this online platform.

    _The overall weight assigned to the Quiz category is 40%._

    [q-and-e]: https://ccle.ucla.edu/course/view.php?id=90004&section=2

*   _Homework assignments:_ homework will be assigned on a weekly basis
    throughout the quarter. You are encouraged to attempt to solve each and
    every single problem assigned during the week but **you will only have to
    submit two problems directly to Gradescope**.

    > Assuming _M_ students are enrolled in this course, and assuming _N_
    > problems were assigned during a week, prior to the deadline of the
    > corresponding assignment, I will generate _M_ lists of 5 random numbers
    > between 1 and _N_. This list will be published at the [Homework
    > assignments][hw] section of this website. Students will then select 2 out
    > of these 5 numbers and will _submit_ the corresponding assignment problems
    > to Gradescope.

    [hw]: ../assignments/

    The submitted problems will be graded based on _accuracy_, _form_, and
    _difficulty_. Easier problems will be graded harshly. Difficult problems
    will be graded more leniently. Note that you can choose to submit any two
    problems from the list assigned to you (even the easiest ones), but you are
    still expected to at least have a pretty good idea of how to solve the rest
    of the problems, as they could be the basis for some of the quizzes. 

    For the first 2 weeks, you will be allowed to submit a picture of your
    written work. After week 2 you will be required to _type_ your submissions
    directly on the Gradescope platform. Guidance will be provided to you to
    familiarize you with the $\LaTeX$ document preparation system.

    **No late homework will be accepted** and **all of your homework scores will
    be used** in the computation of your overall score.

    _The overall weight assigned to the Homework category is 20%._

*   _Midterm:_ two midterms will be given on
    **August 17 (Monday week 3)**, and **August 31 (Monday week 5)**.

    Depending on its number of questions, an exam could be split up into several
    individually timed components. These components will be listed as
    _available_ at CCLE starting at 5 pm PDT on the days listed above. Students
    will then have 24 hours to start all components of an exam. 

    The exam/quiz submission process consists of two steps:

    1. Submission of _small file size_ pdf/image to CCLE within the time
       allotted to every exam component; and

    2. Submission of either a set of high quality image(s), or a clearly legible
       _.pdf_ file to Gradescope. This part of the submission process should be
       completed withih 48 hours of the completion of all components of the
       exam.

       > Failure to complete step 2 above might result in a _tardy_ penalty
       > of 20% or more applied to your score.

    _The overall weight assigned to every individual Midterm is 15%._

*   _Final exam:_ this cumulative exam will be _made available at CCLE_ on
    Wednesday, September 9 at 5:00 pm PDT. Unless indicated otherwise by the
    math department, the logistics will be similar to the one described for
    midterms. _E.g.,_ students must start all components of their exam within 24
    hours of it being made available to the class.

    _The overall weight assigned to the Final category is 10%._

    > **Important:**  
    >  
    > _Failure to submit the final exam through the proper online platforms will
    > result in an automatic F!_

Your final score in the class will be determined by the following grading
breakdown:

|                                                                         |  
|:-----------------------------------------------------------------------:|  
| 40% **Quiz** + 20% **Hw** + 15% **Mid1** + 15% **Mid2** + 10% **Final** |  

Overall scores determine letter grades according to the table below.

|                       |                      |                    |  
|:----------------------|:---------------------|:-------------------|  
| A+ (N/A)              | A (93.33% or higher) | A- (90% -- 93.32%) |  
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |  
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |  

The remaining grades will be assigned at my own discretion. Please do not
request exceptions.

> **Note:** students taking this class on a P/NP basis that attain an
> overall score in the C- range **are not** guaranteed a P letter grade.

**All grades are final when filed by the instructor on the Final Grade Report.**


### Policies and procedures

#### Exams

**There will be no makeup midterms under any circumstances**, but I reserve the
right to apply alternate grading schemes on special circumstances. These
alternate grading schemes might include additional categories including but not
limited to: oral examinations and/or extra project assignments.

Exams will not be returned to you as you will not physically hand them over.
However, _Gradescope_ has a _publish grades_ feature that allows instructors to
_hand back_ exams. Once this feature is invoked, students can, if applicable,
request a regrade. When requesting a regrade, you should be able to clearly
explain what mistake was made (if any), and why your argumentation is correct.


#### Assignments

Homework will be assigned on a weekly basis, and it will be collected through
Gradescope (no CCLE submission needed). Homework extensions are granted
automatically. However, 48 hours after the original deadline, a _tardy_ penalty
of at least 20% will be applied to the corresponding score.


#### Official scores

Towards the end of the quarter, scores from Gradescope will be copied to
[MyUCLA][my-ucla]. The scores appearing on MyUCLA are to be regarded as the
official ones in case of conflicting records. It is your responsibility to
verify in a timely manner that the scores appearing therein are accurate.

[my-ucla]: https://my.ucla.edu/


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: https://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

