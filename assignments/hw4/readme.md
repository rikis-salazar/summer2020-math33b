# Homework 4

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001003

**Due date:** Monday, August 31.

* Quiz 6 (Week 4, Thursday afternoon PDT) is based on questions 1--10 from this
  assignment.

* Midterm 2 (Week 5, Monday afternoon PDT) is based on the final versions of
  homework assignments 3 and 4.

---

## Regular exercises

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

[ref2]: http://ramanujan.math.trinity.edu/wtrench/texts/TRENCH_DIFF_EQNS_I.PDF

Section 5.2: _Constant coefficient homogeneous equations_

1.  Exercises 5.2.2, 5.2.6, 5.2.7.

1.  Exercises 5.2.5, 5.2.9, 5.2.10.

1.  Exercises 5.2.11, 5.2.12, 5.2.16.

1.  Exercises 5.2.13, 5.2.14, 5.2.15.


**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section: _Linear homogeneous equations with constant coefficients_ 

5.  Find the general solution of each of the following equations:

    i.  $y'' + y = 0$
    i.  $y'' + 2y' + 2y = 0$

    > This problem is adapted from exercises 4.3.9, and 4.3.16 in _Polking_.

5.  Find the general solution of each of the following equations:

    i.  $y'' + 2y' + 17y = 0$
    i.  $y'' + 2y' = 0$

    > This problem is adapted from exercises 4.3.12, and 4.3.13 in _Polking_.

5.  Find the general solution of each of the following equations:

    i.  $y'' - 6y' + 9y = 0$
    i.  $16y'' + 24y' + 9y = 0$

    > This problem is adapted from exercises 4.3.18, and 4.3.23 in _Polking_.

5.  Find the general solution of each of the following equations:

    i.  $4y'' + 12y' + 9y = 0$
    i.  $16y'' + 8y' + y = 0$

    > This problem is adapted from exercises 4.3.20, and 4.3.21 in _Polking_.

5.  Given that the characteristic equation $\lambda^{2} + p\lambda  +q = 0$ has
    a double root, $\lambda = \lambda_{1}$, show, by direct substitution, that
    $y(t) = t \mathrm{e}^{\lambda_{1}t}$ is a solution of $y'' + py' + qy = 0$.

    > This problem is ~~adapted~~ _lifted_ from exercises 4.3.38 in _Polking_.

Section: _Harmonic motion_

10. Prove that a _critically damped_ solution of $my'' + by' + ky = 0$ can cross
    the time axis no more than once, regardless of the initial conditions.

    The critically damped case occurs when there is a single repeated root of
    the characteristic polynomial.

    > This problem is adapted from exercises 4.4.19 in _Polking_.

Section: _Inhomogeneous equations; the method of undeteremined coefficients_

11. Find a particular solution for the given differential equations

    i.  $y'' + 3y' + 2y = 4\mathrm{e}^{-3t}$
    i.  $y'' + 3y' - 18y = 18\mathrm{e}^{2t}$

    > This problem is adapted from exercises 4.5.1, and 4.5.4 in _Polking_.

11. Repeat the previous exercise for the following differential equations

    i.  $y'' + 6y' + 8y = -3\mathrm{e}^{-t}$
    i.  $y'' + 2y' - 5y = 12\mathrm{e}^{-t}$

    > This problem is adapted from exercises 4.5.2, and 4.5.3 in _Polking_.

11. Find a particular solution for the given differential equations

    i.  $y'' + 4y = \cos(3t)$
    i.  $y'' + 7y' + 10y = -4\sin(3t)$

    > This problem is adapted from exercises 4.5.5, and 4.5.8 in _Polking_.

11. Repeat the previous exercise for the following differential equations

    i.  $y'' + 9y = \sin(2t)$
    i.  $y'' + 7y' + 6y = 3\sin(2t)$

    > This problem is adapted from exercises 4.5.6, and 4.5.7 in _Polking_.

11. Find the solution of the following _Initial Value Problems_

    i.  $y'' - 4y' - 5y = 4\mathrm{e}^{-2t},\qquad y(0)=0\,\qquad y'(0)=-1$.
    i.  $y'' + 4y' + 4y = 4 - t,\qquad y(0)=-1\,\qquad y'(0)=0$.

    > This problem is adapted from exercises 4.5.19, and 4.5.22 in _Polking_.

11. Repeat the previous exercise for the following _Initial Value Problems_

    i.  $y'' + 2y' + 2y = 2\cos(2t),\qquad y(0)=-2\,\qquad y'(0)=0$.
    i.  $y'' - 2y' + 5y = 3\cos(t),\qquad y(0)=0\,\qquad y'(0)=-2$.

    > This problem is adapted from exercises 4.5.20, and 4.5.21 in _Polking_.

17. Find a particular solution for the given differential equations

    i.  $y'' - 3y' - 10y = 3\mathrm{e}^{-2t}$
    i.  $y'' + 4y = 4\cos(2t)$
    i.  $y'' + 9y = \sin(3t)$

    > This problem is adapted from exercises 4.5.24, 4.5.26, and 4.5.27 in _Polking_.

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

Section 5.5: _The method of undetermined coefficients II_

18. Solve the following _Initial Value Problem_
    $$y'' - 4y' + 4y = 6\mathrm{e}^{2x} + 25\sin(x),\qquad y(0)=5,\qquad y'(0)=3.$$

    > This problem is ~~adapted~~ lifted from exercise 5.5.32 in _Trench_.

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section: _Variation of parameters_

19. Find a particular solution for the given differential equations

    i.  $y'' + 9y = \tan(3t)$
    i.  $x'' + x = \sec^{2}(t)$

    > This problem is adapted from exercises 4.6.1, and 4.6.8 in _Polking_.

19. Repeat the previous exercise for the following differential equations

    i.  $y'' + 4y = \sec(2t)$
    i.  $x'' + x = \sin^{2}(t)$

    > This problem is adapted from exercises 4.6.2, and 4.6.9 in _Polking_.

---

##  Extra exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 4.3

*   Problems 4.3.25, 4.3.26, 4.3.27, 4.3.28, 4.3.29, 4.3.30, 4.3.31, 4.3.32,
    4.3.33, 4.3.34, 4.3.35, 4.3.36.

Section 4.6

*   Problems 4.6.3, 4.6.4, 4.6.5, and 4.6.6.

    > _Note:_
    >
    > These set of problems could be used for practicing undetermined
    > coefficients, as well as variation of parameters. Do your answers from
    > different methods agree? If not, is there something wrong with this?

---

##  Miscellaneous exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 4.4

*   Problem 4.4.17. 

    > _Note:_ the overdamped condition in this case reads $\mu^{2} > 4mk$.

Section 4.5

*   Problem 4.5.9. 
*   Problem 4.5.30. 

Section 4.6

*   Verify that $y_{1}(t) = t$ and $y_{2}(t) = t^{-3}$ are solutions to the
    homogenous equation
    $$t^{2}y''(t) + 3ty'(t) - 3y(t) = 0.$$
    Use variation of parameters to find the general solution to
    $$t^{2}y''(t) + 3ty'(t) - 3y(t) = \frac{1}{t}.$$

    > This problem is ~~adapted~~ lifted from exercises 4.6.13 in _Polking_.


**From [Trench, William F. _Elementary Differential Equations_][ref2].**

*   Exercise 5.2.34 a) b) c).

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

