# Homework assignments

Homework assignment problems fall into three categories: _regular_, _extra_, and
_miscellaneous_ exercises.

*   _Regular_ are the basis for the list of 5 problems that will be assigned to
    you every week. As explained in the [course syllabus][syll], this list will
    be randomly generated and likely different for every student. You should
    select 2 problems out of the 5 in the list and make sure your solutions are
    _accurate_, and that your submissions _look good_. For the first two weeks
    you'll be allowed to submit pictures and/or _pdf_ documents to gradescope,
    but eventually you will be asked to _write your arguments_ directly into the
    Gradescope platform.

    > **Note:** The small-size/high-quality, 2-step procedure described in the
    > syllabus does not apply to homework submissions.

*   _Extra_ exercises are "optional", that is, you do not have to even attempt
    them as they are very similar to the ones you will encounter in the
    _regular_ section. Their purpose is to give you a wider base for you
    practice your skills and/or assimilate the concepts discussed in class. 

    > _Note:_ since these problems are very similar to the ones appearing in the
    > _regular_ section, they could also form the basis for a quiz problem.

*   _Miscellaneous_ exercises are for you to practice key concepts/tools we
    develop during lecture, and/or to help you further explore
    ideas/models/solutions that were briefly discussed during lecture.

    > These problems will not be collected (nor graded), but you are more than
    > welcome to ask me or your TA about them during office hours.

In addition, every homework assignment will be marked with either an _in
progress_, or _final_ status.

*   _In progress (status)_ means that more problems could be added to the
    corresponding assignment. Once the list is complete, the old status will be
    replaced with the _final_ status (see below).
*   _Final (status)_ means that no more problems will be added to the
    corresponding assignment.

---

## Assignment links

*   [Homework 1][h1]: [click this link][list1] to find the random list assigned
    to you for this assignment.

*   [Homework 2][h2]: [click this link][list2] to find the random list assigned
    to you for this assignment.

*   [Homework 3][h3]: [click this link][list3] to find the random list assigned
    to you for this assignment.

*   [Homework 4][h4]: [click this link][list4] to find the random list assigned
    to you for this assignment.

*   [Homework 5][h5]: [click this link][list5] to find the random list assigned
    to you for this assignment.

[h1]: hw1/
[list1]: hw1/random-list.txt
[h2]: hw2/
[list2]: hw2/random-list.txt
[h3]: hw3/
[list3]: hw3/random-list.txt
[h4]: hw4/
[list4]: hw4/random-list.txt
[h5]: hw5/
[list5]: hw5/random-list.txt

<!-- This is also a comment
[list3]: https://www.pic.ucla.edu/~rsalazar/easter-egg.mp4
[list4]: https://www.youtube.com/watch?v=yQq1-_ujXrM
[list5]: https://www.kualo.co.uk/404
-->

---

## Course midterms (base questions)

*   [Midterm 1][mid1]
*   [Midterm 2][mid2]

[mid1]: ./midterm1.pdf
[mid2]: ./midterm2.pdf

---


[Return to main course website][MAIN]

[syll]: ../syllabus/


[MAIN]: ..


