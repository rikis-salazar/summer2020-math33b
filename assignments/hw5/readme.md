# Homework 5

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001003

**Due date:** ~~Wednesday, September 9~~ Friday, September 11 (see class
announcement for the idea behind this extension).

* Quiz 7 (Week 5, Thursday afternoon PDT) is based on questions 11--20 from
  **homework assignment 4**.

* Quiz 8 (Week 6, Tuesday afternoon PDT) is based on questions 1--10 from this
  assignment.

  > **Note:** This quiz will remain _visible_ on CCLE until midnight on Friday,
  > September 11.

---

## Regular exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section: _Linear systems_

1.  Exercises 8.4.1, 8.4.2, 8.4.3, 8.4.4, 8.4.5, 8.4.6.

1.  Exercises 8.4.7, 8.4.8, 8.4.9, 8.4.10.

1.  Exercise 8.4.25.

Section: _Properties of linear systems_

4.  Exercises 8.5.1, 8.5.2, 8.5.3, 8.5.4, 8.5.5, 8.5.6.

4.  Exercises 8.5.7, 8.5.8, 8.5.9, 8.5.10.

4.  Exercise 8.4.27.

Section: _Overview of the technique_

7.  Exercises 9.1.1, 9.1.2, 9.1.3, 9.1.4, 9.1.5, 9.1.6.

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

[ref2]: http://ramanujan.math.trinity.edu/wtrench/texts/TRENCH_DIFF_EQNS_I.PDF

Section 10.4: _Constant coefficient homogeneous systems I_

8.  Exercises 10.4.1, 10.4.2, 10.4.3, 10.4.4.

8.  Exercises 10.4.16, 10.4.19.

Section 10.6: _Constant coefficient homogeneous systems III_

10. Exercises 10.6.1, 10.6.2, 10.6.3, 10.6.4.


---

##  Extra exercises

No extra exercises. See Miscellaneous section for _Constant coefficients
homogeneous systems II_.

---

##  Miscellaneous exercises

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

Section 10.5: _Constant coefficient homogeneous systems II_

*   Exercises 10.5.1, 10.5.2, 10.5.3, 10.5.4.

*   Exercises 10.5.13, 10.5.14.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

