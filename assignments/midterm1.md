# Midterm 1 (base questions)

## Question 1

Consider the differential equation
$$(3x^{2} + y^{2})\,dx + 2xy\,dy = 0.$$

1.  Find a function $F(x,y)$ such that the general solution is given by $F(x,y)
    = C$, for arbitrary real constants $C$.
2.  Find the solution that passes through the point (1,-2) in the $xy$-plane.

> Notes:
>
> *   Randomized question with 4 possible minor variations.


## Question 2

Solve the initial value problem
$$(2x+1)x' = 2t,\qquad x(2) = -1,$$
and determine the interval of existence of the solution.

> Notes:
>
> *   Randomized question with 4 possible minor variations.
> *   See handout 2 in the [Miscellaneous Handouts][hand] section of our math
>     website.

[hand]: https://www.math.ucla.edu/~rsalazar/summer2020-c/handouts/


## Question 3

A tank contains 100,000 ounces of water in which 33 kilograms of salt is
dissolved. Pure water enters the tank at a rate of 2020 ounces per minute. The
mixture, kept uniform by stirring, flows out of the tank at the rate of 2015
ounces per minute.

How much salt is in the tank after $t$ minutes?

> Notes:
>
> *   Non-randomized question.


## Question 4

Solve the following differential equation
$$y^{2}\,dx + (y^{2}x + 2xy - 1)\,dy = 0.$$

> _Hint:_ Choose your independent variable carefully to obtain a linear
> equation.

> Notes:
>
> *   Randomized question with 4 possible minor variations.


## Question 5

Answer the following questions.

1.  Show that the function $y_{1}(t) = \mathrm{e}^{-t}$, solves the initial
    value problem
    $$y' = -y,\qquad y(0)=1$$.

2.  Find the value of the constant $r$ so that the function $y_{2} =
    \mathrm{e}^{rt}$ solves the differential equation
    $$y' + 2y = 0.$$

3.  _Invalid question!_

> Notes:
>
> *   Non-randomized question.
> *   Typo on part 3 of the original post (see below) makes problem impossible
>     to solve.
>
>
> _Original version of the problem:_
>
> 3.  Let $c_{1}$, $c_{2}$, be arbitrary constants and let
>     $$y_{3}(t) = c_{1}y_{1}(t) + c_{2}y_{2}(t),$$
>     where $y_{1}$ and $y_{2}$ are the functions from parts 1 & 2 (above). Show
>     that $y_{3}$ solves the differential equation
>     $$y'' -3y' + 2y = 0.$$

