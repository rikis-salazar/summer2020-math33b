# Homework 3

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001003

**Due date:** Monday, August 24.

* Quiz 4 (Week 3, Thursday afternoon PDT) is based on questions 10--20 from
  **homework assignment 2**.

* Quiz 5 (Week 4, Tuesday afternoon PDT) is based on questions 1--10 from this assignment.

---

## Regular exercises

1.  Find the general solution of each of the following _homogeneous_
    differential equations:

    i.  $(x^{2} + y^{2})\, dx − 2xy\, dy = 0$
    i.  $(3x + y)\, dx + x\, dy = 0$
    i.  $(y + 2x\mathrm{e}^{-y/x})\, dx - x\, dy = 0$

    > This problem is adapted from exercises 2.6.35, 2.6.37, and 2.6.40 in
    > _Polking_.

1.  Suppose that $2y\, dx + (x + y)\, dy = 0$ has an integrating factor that is
    a function of $y$ alone [_i.e.,_ $\mu = \mu(y)$]. Find the integrating
    factor and use it to solve the differential equation.

    > This problem is ~~lifted~~ adapted from exercise 2.6.28 in _Polking_.


**From [Trench, William F. _Elementary Differential Equations_][ref2].**

[ref2]: http://ramanujan.math.trinity.edu/wtrench/texts/TRENCH_DIFF_EQNS_I.PDF

Section 2.6: _Exact equations_

3.  Exercises 2.6.1 a), 2.6.2 a).

3.  Exercises 2.6.3, 2.6.6.

3.  Exercises 2.6.4, 2.6.5.

3.  Exercises 2.6.8, 2.6.9.

    > _Hint:_ In problems 3--6 above, study the functions
    > $$h = \frac{1}{Q}(P_{y}-Q_{x})\qquad\text{and}\qquad
    >   k = \frac{1}{P}(Q_{x}-P_{y}),$$
    > where $P_{y}$ and $Q_{x}$ denote the partial derivative of $P$ with
    > respect to $y$, and the partial derivative of $Q$ with respect to $x$,
    > respectively.

3.  Exercises 2.6.18, 2.6.21.

3.  Exercises 2.6.19, 2.6.20.

Section 2.3: _Existence and uniqueness of solutions of non-linear equations_

9.  Exercises 2.3.2, 2.3.5, 2.3.6, 2.3.8.

9.  Exercise 2.3.19.


**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section: _Existence and uniqueness of solutions_

11. Exercises 2.7.1, 2.7.3, 2.7.4.

11. Exercise 2.7.10.

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

Section 5.1: _Homogeneous linear equations_

13. Exercise 5.1.1 a) b) c).

13. Exercise 5.1.2 a) b) c).

13. Exercise 5.1.3 a) b) c).

Section 5.2: _Homogeneous linear equations_

16. Exercises 5.2.1, 5.2.3, 5.2.8.

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section: _Definition and examples (Second-order equations)_ 

16. For each of the following second-order differential equations, decide
    whether the equation is linear or nonlinear. If it is linear, state whether
    it is homogeneous or inhomogeneous.

    i.  $y'' + 3y' + 5y = 3\cos(2t)$
    i.  $t^{2}y'' = 4y' -\sin t$ 
    i.  $t^{2}y'' + (1-y)y' = \cos(2t)$
    i.  $ty'' + (\sin t)y' = 4y - \cos(5t)$
    i.  $t^{2}y'' + 4yy' = 0$
    i.  $y'' + 4y' + 7y = 3\mathrm{e}^{-t}\sin t$
    i.  $y'' + 3y' + 4\sin y = 0$
    i.  $(1-t^{2})y'' = 3y$

    > This question is based on exercises 4.1.1--4.1.8 in Polking.

16. In the following exercises, show (by direct substitution) that the given
    functions $y_{1}(t)$ and $y_{2}(t)$ are solutions of the given differential
    equation. Then verify again (by direct substitution) that any linear
    combination $C_{1} y_{1}(t) + C_{2} y_{2}(t)$ of the two given solutions is
    also a solution.

    i.  $y'' - y' - 6y = 0,\quad y_{1}(t) = \mathrm{e}^{3t},\quad y_{2}(t) =
        \mathrm{e}^{-2t}$.
    i.  $y'' + 4y = 0,\quad y_{1}(t) = \cos(2t),\quad y_{2}(t) = \sin(2t)$.
    i.  $y'' - 2y' + 2y = 0,\quad y_{1}(t) = \mathrm{e}^{t}\cos t,\quad y_{2}(t)
        = \mathrm{e}^{t}\sin t$.
    i.  $y'' + 4y' + 4y = 0,\quad y_{1}(t) = \mathrm{e}^{-2t} ,\quad y_{2}(t) =
        t\mathrm{e}^{-2t}$.

    > This question is based on exercises 4.1.13--4.1.16 in Polking.

Section: _Linear homogeneous equations with constant coefficients (Second-order equations)_ 

19. Find the general solution of the following ODEs

    i.  $y'' - y' - 2y = 0$
    i.  $y'' + y' - 12y = 0$
    i.  $2y'' - y' - y = 0$
    i.  $6y'' + 5y' - 6y = 0$

    > This question is based on exercises 4.3.1, 4.3.4, 4.3.5, 4.3.8 in Polking.

19. Suppose that $\lambda_{1}$ and $\lambda_{2}$ are roots (zeros) of the
    characteristic equation $\lambda^{2} + p\lambda + q = 0$, where $p$ and $q$
    are real constants.

    i.  Prove that $\lambda_{1}\lambda_{2} = q$.
    i.  $\lambda_{1} + \lambda_{2} = -p$.

    > This question is based on exercise 4.3.37 in Polking.

---

##  Extra exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 2.6

*   Problems 2.6.22, 2.6.23, 2.6.24, 2.6.25, 2.6.26, 2.6.29, 2.6.31, 2.6.32,
    2.6.33, 2.6.34, 2.6.36, 2.6.38, 2.6.39.

Section 2.7

*   Problems 2.7.7, 2.7.8.

---

##  Miscellaneous exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 2.7

*   Problems 2.7.27, 2.7.28.

*   Consider the IVP
    \begin{equation}
      y' = 3y^{2/3},\qquad y(0) = 0.
    \end{equation}
    It is not difficult to construct an infinite number of solutions. Consider
    \begin{equation}
      y(t) =
        \begin{cases}
          0, & \text{if } t > t_{0}, \\
          (t-t_{0})^{3}, & \text{if } t > t_{0},
        \end{cases}
    \end{equation}
    where $t_{0}$ is any positive number. It is easy to calculate the derivative
    of $y(t)$, when $t\neq t_{0}$,
    \begin{equation}
      y'(t) =
        \begin{cases}
          0, & \text{if } t > t_{0}, \\
          3(t-t_{0})^{2}, & \text{if } t > t_{0},
        \end{cases}
    \end{equation}
    but the derivative at $t_{0}$ remains uncertain.

    i.  Evaluate both
        $$y'(t_{0}^{+}) = \lim_{t\to 0^{+}} \frac{y(t)-y(t_{0})}{t-t_{0}}$$
        and
        $$y'(t_{0}^{-}) = \lim_{t\to 0^{-}} \frac{y(t)-y(t_{0})}{t-t_{0}},$$
        showing that
        \begin{equation}
          y'(t) =
            \begin{cases}
              0, & \text{if } t \le t_{0}, \\
              3(t-t_{0})^{2}, & \text{if } t > t_{0}.
            \end{cases}
        \end{equation}

    i.  Explain why all of the infinitely many $y(t)$ functions constructed in
        the previous part ---one for every different value $t_{0}$--- is a
        solution of the IVP. Why doesn't this example contradict the uniqueness
        theorem 7.16 in _Polking_?

        > See page 12 on [this set of _slides_][slides] for a statement of the
        > theorem referenced above.

    > This problem is adapted from exercise 2.7.21.

[slides]: ../../notes/03-tue.pdf


**From [Trench, William F. _Elementary Differential Equations_][ref2].**

*   Exercise 5.1.1 d).

*   Exercise 5.1.2 d).

*   Exercise 5.1.3 d).

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

