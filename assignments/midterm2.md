# Midterm 2 (base questions)

## Question 1

Find the general solution of the first order differential equation
$$y' = -\frac{x}{y} - \frac{y}{x}.$$

It is OK to express your answer implicitly.

> _Hint:_ What happens if you replace $x$ and $y$ with $tx$ and $ty$ for an
> arbitrary value of $t$?

> Notes:
>
> *   This is a _homogeneous_ differential equation of degree 0.
> *   Randomized question with 2 possible minor variations.


## Question 2

Consider the differential equation
$$P(\eta,\theta)\,\mathrm{d}\theta + Q(\eta,\theta)\,\mathrm{d}\eta = 0,
\qquad(\mathrm{I})$$
where $P$, $Q$, and their partial derivatives are continuous everywhere inside a
rectangle $R$ in the $\eta\theta$-plane.

Answer **true** or **false**. You **do not need** to justify your answer.

1.  If $P(\eta,\theta) = a(\theta)b(\eta)$ and $Q(\eta,\theta) =
    c(\eta)d(\theta)$, then equation ($\mathrm{I}$) above is separable. Moreover
    $\mu(\eta,\theta) = \frac{k^{2}+1}{b(\eta)d(\theta)}$ is an integrating
    factor, regardless of the choice of the real constant $k$.

1.  Equation ($\mathrm{I}$) above is exact if and only if $\frac{\partial
    Q}{\partial\theta} = \frac{\partial P}{\partial\eta}$ everywhere on $R$. In
    this case, it is possible to find $F = F(\eta,\theta)$ such that
    $\frac{\partial F}{\partial \theta} = P$ and $\frac{\partial F}{\partial
    \eta} = Q$.

1. Let $M>0$ and let $(\eta_{0},\theta_{0})$ be a point in $R$. If everywhere
   on $R$, $f(\eta,\theta)$ and $\frac{\partial f}{\partial\theta}$ are
   continuous, then there exists a unique solution for the Initial Value Problem
   $\theta' = f(\eta,\theta)$, $\theta(\eta_{0}) = \theta_{0}$.

> Notes:
>
> *   Non-randomized question.
> *   The constant $M$ in part 3 is a _leftoever_ from a question I used last
>     year. In that case, the assumption was a bit weaker: namely, that the
>     derivative with respect to the dependent variable was bounded by $M$ in
>     the rectangle $R$ (and not necessarily continuous therein). This _typo_
>     does not affect the answer of this part of the problem.

[hand]: https://www.math.ucla.edu/~rsalazar/summer2020-c/handouts/


## Question 3

Find one particular solution of the differential equation
$$y'' + 4y = 3\cos(2t) - 8\mathrm{e}^{2t}$$

> _Hint:_
>
> Consider finding particular solutions $y_{p_{1}}$ and $y_{p_{2}}$ of the
> differential equations
> $$y'' + 4y = 3\cos(2t),\qquad\text{and}\qquad y''+4y = -8\mathrm{e}^{2t},$$
> respectively. The answer to this problem would then be $y_{p} = y_{p_{1}} +
> y_{p_{2}}$.

> Notes:
>
> *   Non-randomized question.


## Question 4

Using **variation of parameters**, find the general solution of
$$y'' + y = x.$$

> _Important:_
>
> The goal of this exercise is to test your ability to use _variation of
> parameters_. Very little credit will be awarded if you decide to solve this
> problem via undetermined coefficients.

> Notes:
>
> *   Randomized question with 2 possible minor variations.


## Question 5

Find the general solution of the third order equation with constant coefficients
$$ y''' + 4 y'' + 4y' = 0.\qquad(\mathrm{II})$$

>  _Hints:_
>
> *   If you are not comfortable applying the techniques you learned for
>     differential equations with constant coefficients to this higher order
>     equation, integrate equation ($\mathrm{II}$) with respect to $t$ to obtain
>     the 2nd order nonhomogeneous equation with constant coefficients
>     $$y'' + 4 y' + 4y = c_{1},$$
>     where $c_{1}$ is an arbitrary constant.
> *   The general solution to this third order equation should depend on three
>     constants, say $c_{1}$, $c_{2}$, and $c_{3}$.

> Notes:
>
> *   Non-randomized question.

