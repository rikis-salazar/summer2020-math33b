# Final (base questions)

## Question 0

Consider the differential equation

\\( P(\eta,\theta)\,\mathrm{d}\theta + Q(\eta,\theta)\,\mathrm{d}\eta = 0,\qquad(\star) \\)

where \\(P\\), \\(Q\\), and their partial derivatives, are continuous everywhere inside a rectangle \\(R\\) in the \\(\eta\theta\\)-plane.

Answer **true** or **false**. You **do not** need to justify your answer.

1. It is possible to select \\(P(\eta,\theta)\\) and \\(Q(\eta,\theta)\\) in equation (\\(\star\\)) so that the differential equation is exact but not separable.

2. If \\(P(\eta,\theta) = a(\theta)b(\eta)\\) and \\(Q(\eta,\theta)= c(\eta)d(\theta)\\) then equation (\\(\star\\)) is separable. Moreover \\(\mu(\eta,\theta) = \frac{1}{a(\theta)c(\eta)}\\) is a possible integrating factor.  

3. If \\(h = \frac{1}{P} \big( \frac{\partial Q}{\partial\theta} - \frac{\partial P}{\partial\eta} \big)\\) depends only on \\(\eta\\) \\(\big(\\)i.e., \\(h = h(\eta)\big)\\) then \\(\mu(\eta) = \mathrm{e}^{\int h(\eta) \mathrm{d}\eta}\\) is an integrating factor of equation (\\(\star\\)).  

4. Equation (\\(\star\\)) is exact if and only if \\(\frac{\partial Q}{\partial\theta} = \frac{\partial P}{\partial\eta}\\) everywhere on \\(R\\) In this case, it is possible to find \\(F=F(\eta,\theta)\\) such that \\(\frac{\partial F}{\partial \eta} = P\\) and \\(\frac{\partial F}{\partial \theta} = Q\\).



## Question 1

Some nonlinear equations can be transformed into linear equations by changing the dependent variable. Show that if

\\( g'(y)y' + p(x)g(y) = f(x) \\)

where \\(y\\) is a function of \\(x\\), and \\(g\\) is a function of \\(y\\), then the new dependent variable \\(z=g(y)\\) satisfies the linear equation

\\( z' + p(x)z = f(x). \\)

## Question 2

Occasionally a differential equation with coefficients that are _almost homogeneous_ can be rewritten in _homogeneous_ form by use of simple transformations.

Consider the differential equation

\\( (x-y-1) \mathrm{d}x + (x+4y-6) \mathrm{d}y = 0.\qquad(\mathrm{I}) \\)

1. Show that it is possible to choose constants \\(a\\) and \\(b\\) so that the transformations \\(x = u + a\\) and \\(y = v + b\\) turn equation (\\(\mathrm{I}\\)) into the homogeneous differential equation

   \\( (u-v)\mathrm{d}u + (u+4v)\mathrm{d}v = 0.\qquad(\mathrm{II}) \\)

2. Show that the general solution of equation (\\(\mathrm{II}\\)) is

   \\( \log(u^{2} + 4v^{2}) = \tan^{-1}\Big(\frac{u}{2v}\Big) + C. \\)

> _Note:_
>
> You can still get credit for the second part of this problem even if you do not complete the first one.


## Question 3

Find all possible pairs of real numbers \\((a,b)\\) such that

\\( \mu(x,y) = x^{a}y^{b} \\)

is an integrating factor of

\\( 2y\,\mathrm{d}x  + 3x\,\mathrm{d}y = 0. \\)

Next, choose two different such pairs and find the general solutions of the above differential equation associated to these pairs. Verify that even though the integrating factors are different, the general solution remains the same.


## Question 4

1. Let \\(r\_{1}\\) and \\(r\_{2}\\) be two different real numbers. Show that if \\(t\neq 0\\), the _Wronskian_ of \\(t^{r\_{1}}\\) and \\(t^{r\_{2}}\\) is different from zero.

2. Find the general solution of the second order equation with non-constant coefficients

   \\( t^{2}y''(t) + 3t y'(t) - 3y(t) = \frac{1}{t},\quad t>0. \\)

   > _Hints:_
   >
   > * Propose solutions of the homogeneous problem of the form \\(y(t) = t^{r}\\) then use variation of parameters to obtain a particular solution of the inhomogeneous problem.
   > * What does part 1 tell you about the general solution of the homogeneous problem?


## Question 5

Consider the set of differential equations

\\( \begin{align} 4 y\_{1}'(t) & = -7y\_{1}(t) +  y\_{2}(t),\\\\ 4 y\_{2}'(t) & = -5y\_{2}(t) + 3y\_{1}(t). \end{align} \\)

Find the solution of the system that satisfies the initial conditions \\(y\_{1}(0) = y\_{2}(0) = 4\\).



