# Homework 1

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001003

**Due date:** Monday, August 10.

* Quiz 1 (Week 1, Thursday afternoon PDT) is based on questions 1--10.

* Quiz 2 (Week 2, Tuesday afternoon PDT) is based on questions 11--20.

---

## Regular exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section: _Differential Equation Models_

1.  In the following exercises, model each application with a differential
    equation. All rates are assumed to be with respect to time.

    i.  The rate of growth of bacteria in a petri dish is proportional to the
        number of bacteria in the dish.
    i.  The rate of growth of a population of field mice is inversely
        proportional to the square root of the population.

Section: _Differential Equations and Solutions_

2.  In the following exercises, given the function $\phi$, place the ordinary
    differential equation $\phi(t,y,y') = 0$ in normal form.

    i.  $\phi(x,y,z) = x^{2}z + (1+x)y$
    i.  $\phi(x,y,z) = xz − 2y − x^{2}$

2.  A general solution may fail to produce all solutions of a differential
    equation. Consider the differential equation
    $$y'=y(4−y).$$

    i.  Show that $y(t)=0$ is a solution of the above differential equation.
    i.  Show that for any $C$, the familiy of functions
        $$y(t) = \frac{4}{1+C\mathrm{e}^{−4t}}$$
        are also solutions of the above equation, but no value of $C$ produces
        the solution from the previous part.

2.  
    i.  Use implicit differentiation to show that $t^{2} + y^{2} = C^{2}$
        implicitly defines solutions of the differential equation $t + yy' = 0$.
    i.  Solve $t^{2} + y^{2} = C^{2}$ for $y$ in terms of $t$ to provide
        explicit solutions. Show that these functions are also solutions of $t +
        yy' = 0$.

2.  Show that
    $$y(t) = \frac{3}{6t-11}$$
    is a solution of
    $$y'=-2y^{2},\quad y(2)=3.$$

Section: _Solutions to Separable Equations_

6.  In the following exercises, find the general solution of the indicated
    differential equation. If possible, find an explicit solution.

    i.  $y' = \mathrm{e}^{x-y}$
    i.  $y' = \dfrac{xy}{x-1}$

6.  Repeat exercise 6 above with the following differential equations:

    i.  $y' = (1+y^{2})\mathrm{e}^{x}$
    i.  $y' = \dfrac{x}{y+2}$

6.  In the following exercises, find the exact solution of the initial value
    problem. Indicate the interval of existence.

    i.  $y' = \dfrac{y}{x},\quad y(1) = -2$.
    i.  $y' = \dfrac{-2t(1+y^{2})}{y},\quad y(0)=1$.

6.  Suppose that a radioactive substance decays according to the model
    $$N(t) = N_{0}\mathrm{e}^{-\lambda t}.$$
    Show that the _half-life_ of the radioactive substance is given by the
    equation
    $$T_{1/2} = \frac{\log 2}{\lambda}.$$

    > _Notes:_
    >
    > *   According to the course textbook, the **half-life** of a radioactive
    >     substance is the amount of time required for 50% of the substance to
    >     decay.
    >
    > *   There is a typo on the textbook. Exercise 2.2.23, uses the formula
    >     $N'= N_{0}\mathrm{e}^{-\lambda t}$. The correct formula is the one
    >     listed above.

6.  A murder victim is discovered at midnight and the temperature of the body is
    recorded at 31$^{\circ}$C. One hour later, the temperature of the body is
    29$^{\circ}$C. Assume that the surrounding air temperature remains constant
    at 21$^{\circ}$C. Use _Newton’s law of cooling_ to calculate the victim’s
    time of death.

    > _Note:_
    >
    > The “normal” temperature of a living human being is approximately
    > 37$^{\circ}$C.

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

[ref2]: http://ramanujan.math.trinity.edu/wtrench/texts/TRENCH_DIFF_EQNS_I.PDF

Section 1.2: _First order equations_

11. Exercise 1.2.1.

11. Exercise 1.2.2 a) d).

11. Exercise 1.2.2 b) c).

11. Exercise 1.2.8.

Section 2.2: _Separable equations_

15. Exercises 2.2.1, 2.2.6.

15. Exercises 2.2.2, 2.2.3.

15. Exercise 2.2.11.

15. Exercise 2.2.12.

15. Exercise 2.2.19.

15. Exercise 2.2.20.

---

##  Extra exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 1.1

*   Problems 1.1.4, and 1.1.5.

Section 2.1

*   Problem 2.1.9 a) b).
*   Show that $y(t) = \frac{4}{1-5\mathrm{e}^{-4t}}$ is a solution to the
    initial value problem $y' = y(4-y)$, $y(0) = -1$.

Section 2.2

*   Problem 2.2.2.
*   Problem 2.2.11.
*   Problem 2.2.16.
*   Problem 2.2.17.
*   Problem 2.2.34.

---

##  Miscellaneous exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 2.1

*   Problems 2.1.4, and 2.1.5.

Section 2.2

*   Problems 2.2.36.


**From [Trench, William F. _Elementary Differential Equations_][ref2].**

*   Exercise 1.2.9.

*   Exercise 2.2.27.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

