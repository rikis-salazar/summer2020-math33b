# Homework 2

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=3001003

**Due date:** Monday, August 17.

* Quiz 3 (Week 2, Thursday afternoon PDT) is based on questions 1--10.

* Midterm 1 (Week 3, Monday afternoon PDT) is based on the final versions of
  assignments 1 and 2.

---

## Regular exercises

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

[ref2]: http://ramanujan.math.trinity.edu/wtrench/texts/TRENCH_DIFF_EQNS_I.PDF

Section 2.1: _Linear first order equations_

1.  Exercises 2.1.2, 2.1.5.

1.  Exercises 2.1.3, 2.1.4.

1.  Exercises 2.1.6, 2.1.9.

1.  Exercises 2.1.7, 2.1.8.

1.  Exercises 2.1.18, 2.1.21.

1.  Exercises 2.1.19, 2.1.20.

1.  Exercises 2.1.31, 2.1.32.

1.  Exercise 2.1.43.


**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section: _Linear Equations_

9.  i.  The differential equation $y' + y\cos x = \cos x$ is _linear_. Use the
        _integrating factor_ technique to find the general solution.

    i.  The differential equation $y' + y\cos x = \cos x$ is also _separable_.
        Use the separation of variables technique to solve the equation and
        discuss any discrepancies (if any) between this solution and the
        solution you found in the previous part.

    > This problem is adapted from exercise 2.4.13.

9.  In the following exercises, find the solution of the initial value problem
    and determine its interval of existence.

    i.  $xy' + 2y = \sin x,\qquad y(\pi/2) = 0$.
    i.  $(2x +3)y' = y + (2x + 3)^{1/2},\qquad y(-1) = 0$.

    > This problem is adapted from exercises 2.4.18 and 2.4.19.

Section: _Mixing problems_

11. A tank contains 100 gal of pure water. At time zero, a sugar-water solution
    containing 0.2 lb of sugar per gal enters the tank at a rate of 3 gal per
    minute. Simultaneously, a drain is opened at the bottom of the tank allowing
    the sugar solution to leave the tank at 3 gal per minute. Assume that the
    solution in the tank is kept perfectly mixed at all times.

    i.  What will be the sugar content in the tank after 20 minutes?

    i.  How long will it take the sugar content in the tank to reach 15 lb?

    i.  What will be the eventual sugar content in the tank? In other words, if
        $x(t)$ denotes the sugar content in the tank at time $t$, what is the
        limit as $t\to\infty$ of $x(t)$?

    > This problem is adapted from exercise 2.5.1.

11. A 50-gal tank initially contains 20 gal of pure water. Salt-water solution
    containing 0.5 lb of salt for each gallon of water begins entering the tank
    at a rate of 4 gal/min. Simultaneously, a drain is opened at the bottom of
    the tank, allowing the salt-water solution to leave the tank at a rate of 2
    gal/min. What is the salt content (lb) in the tank at the precise moment
    that the tank is full of salt-water solution?

    > This problem is adapted from exercise 2.5.5.


**From [Trench, William F. _Elementary Differential Equations_][ref2].**

13. Exercises 2.5.1, 2.5.13.

13. Exercises 2.5.4, 2.5.11.

13. Exercises 2.5.18, 2.5.21.

13. Exercises 2.5.19, 2.5.20.

13. Exercise 2.5.29.

13. Exercise 2.5.33.

13. Exercise 2.5.34.

13. Solve the equation
    $$(y\mathrm{e}^{xy}\tan x +\mathrm{e}^{xy}\sec^{2}x)\,dx + 
      x\mathrm{e}^{xy}\tan x\,dy = 0.$$

    > _Hint:_ Try partial integration with respect to $y$ first.

---

##  Extra exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 2.4

*   Problems 2.4.1, 2.4.2, 2.4.3, 2.4.6, 2.4.11, 2.4.14, 2.4.15, 2.4.16.

Section 2.5

*   Problems 2.5.2, 2.5.3 a).

**From [Trench, William F. _Elementary Differential Equations_][ref2].**

*   Problems 4.2.8, 4.2.9.

*   Problem 2.5.30.

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 2.6

*   Problems 2.6.1, 2.6.4, 2.6.6, 2.6.7, 2.6.9, 2.6.10, 2.6.11, 2.6.12, 2.6.13,
    2.6.14, 2.6.22, 2.6.23.

---

##  Miscellaneous exercises

**From Polking, Boggess & Arnold. Differential Equations (2nd edition).**

Section 2.4

*   Problems 2.4.22, and 2.4.23.

Section 2.5

*   Problems 2.5.14.


**From [Trench, William F. _Elementary Differential Equations_][ref2].**

*   Problems 2.1.47, and 2.1.48 a).

*   Problem 4.2.15.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

